import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

const gender = [{ value: "Laki-laki" }, { value: "Perempuan" }];

const category = [
  { value: "Musisi" },
  { value: "Model" },
  { value: "MC" },
  { value: "Lainnya" },
];

const genre = [
  { value: "Blues" },
  { value: "Rock" },
  { value: "Klasik" },
  { value: "Alternative" },
];

const religion = [
  { value: "Islam" },
  { value: "Kristen" },
  { value: "Katolik" },
  { value: "Hindu" },
  { value: "Buddha" },
  { value: "Lainnya" },
];

const location = [
  { value: "Jakarta" },
  { value: "Bekasi" },
  { value: "Depok" },
  { value: "Tangerang" },
  { value: "Surabaya" },
];

const status = [
  { value: "Single" },
  { value: "Menikah" },
  { value: "Sudah berkeluarga" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      //   marginTop: 16,
      // width: "31ch",
      borderColor: "black",
    },
  },
}));

const CustomTextField = withStyles({
  root: {
    marginTop: 4,
    width: 242,
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        // width: "27ch",
        width: "100%",
        borderColor: "#000000",
        borderRadius: 10,
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
})(TextField);

export default function DropdownTalent(props) {
  const classes = useStyles();

  var pilihan = (item) => {
    if (item === "gender") {
      return gender;
    } else if (item === "category") {
      return category;
    } else if (item === "genre") {
      return genre;
    } else if (item === "religion") {
      return religion;
    } else if (item === "location") {
      return location;
    } else if (item === "status") {
      return status;
    }
  };

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <Autocomplete
          id="combo-box-demo"
          options={pilihan(props.type)} //locations ///posts
          getOptionLabel={(option) => option.value}
          renderInput={(params) => (
            <CustomTextField
              {...params}
              placeholder="Pilihan"
              // label="Cari Lokasi"
              variant="outlined"
              size="small"
            />
          )}
          size="small"
          fullWidth
        />
      </div>
    </form>
  );
}
