import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

const typeVenue = [
  { value: "Hall" },
  { value: "Wedding Hall" },
  { value: "Auditorium" },
  { value: "Lainnya" },
];

const typeVendor = [
  { value: "Catering" },
  { value: "Makeup Artist" },
  { value: "Fotografer" },
  { value: "Videografer" },
];

const location = [
  { value: "Jakarta" },
  { value: "Bekasi" },
  { value: "Depok" },
  { value: "Tangerang" },
  { value: "Surabaya" },
];

const gender = [{ value: "Laki-laki" }, { value: "Perempuan" }];

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      //   marginTop: 16,
      // width: "31ch",
      borderColor: "black",
    },
  },
}));

const CustomTextField = withStyles({
  root: {
    marginTop: 4,
    width: 242,
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        // width: "27ch",
        width: "100%",
        borderColor: "#000000",
        borderRadius: 10,
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
})(TextField);

export default function DropdownVenue(props) {
  const classes = useStyles();

  var pilihan = (item) => {
    if (item === "type-1") {
      return typeVenue;
    } else if (item === "type-2") {
      return typeVendor;
    } else if (item === "location") {
      return location;
    } else if (item === "gender") {
      return gender;
    }
  };

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <Autocomplete
          id="combo-box-demo"
          options={pilihan(props.type)} //locations ///posts
          getOptionLabel={(option) => option.value}
          renderInput={(params) => (
            <CustomTextField
              {...params}
              placeholder="Pilihan"
              // label="Cari Lokasi"
              variant="outlined"
              size="small"
            />
          )}
          size="small"
          fullWidth
        />
      </div>
    </form>
  );
}
