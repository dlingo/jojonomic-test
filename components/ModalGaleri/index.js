import { makeStyles } from "@material-ui/core/styles";
import Cross from "../Icons/CroosIcon";
import { Dialog, IconButton } from "@material-ui/core";
import SwiperGaleriModal from "../Swiper/swipergalerimodal";

const useStyles = makeStyles((theme) => ({
  paperWidthSm: {
    maxWidth: 900,
    margin: 0,
  },
}));

const DialogTitle = (props) => {
  const { children, onClose, ...other } = props;
  return (
    <div
      className="xs:px-3 sm:px-5 md:px-6 lg:px-8 xl:px-8 xs:py-0 md:py-2 lg:py-6 bg-primary-orange z-10"
      {...other}
    >
      {onClose ? (
        <IconButton
          aria-label="close"
          className="absolute float-right mx-5"
          style={{ color: "white" }}
          onClick={onClose}
        >
          <Cross className="xs:w-5 lg:w-7" />
        </IconButton>
      ) : null}
    </div>
  );
};

export default function ModalGaleri(props) {
  const classes = useStyles();
  return (
    <Dialog
      fullWidth
      open={props.openModal}
      onClose={props.handleClickCloseModal}
      aria-labelledby="customized-dialog-title"
      classes={{
        paperWidthSm: classes.paperWidthSm,
      }}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={props.handleClickCloseModal}
      ></DialogTitle>
      <div>
        <SwiperGaleriModal />
      </div>
      <div className="bg-primary-orange ">
        <div className="grid grid-cols-2 gap-3 xs:mt-3 lg:mt-[34px] xs:mb-2 lg:mb-4 xs:mx-2 lg:mx-14">
          <div className="text-white font-bold xs:text-xs lg:text-4xl">
            Ruangan Komputer
          </div>
          <div className="text-right xs:text-xs lg:text-2xl text-white">
            Rp 12.000.000
          </div>
        </div>
        <div className="flex xs:mx-2 lg:mx-14 text-white xs:text-xs lg:text-2xl xs:mb-3 lg:mb-7">
          <span className="font-bold">Kapasitas&nbsp;</span>
          <span>100 Orang&nbsp;</span>
          <span className="font-bold">luas&nbsp;</span>
          <span>100 m2&nbsp;</span>
        </div>
      </div>
    </Dialog>
  );
}
