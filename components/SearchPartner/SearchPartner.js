import React, { useState } from "react";

import TextField from "components/TextField";
import ListBox from "components/ListBox";
import InputDatePicker from "@/components/DatePicker";
import InputSlider from "components/InputSlider";
import InputRating from "components/Rating";
import Button from "components/Button";

import Grid from "@material-ui/core/Grid";

// end style material ui

export default function CardSearchPartner(props) {
  const partner = [
    { name: "talent" },
    { name: "influencer" },
    { name: "vendor" },
    { name: "venue" },
  ];

  const dummyCategory = [
    { name: "Category1" },
    { name: "Category2" },
    { name: "Category3" },
    { name: "Category4" },
  ];

  const dummyLokasi = [
    { name: "lokasi1" },
    { name: "lokasi2" },
    { name: "lokasi3" },
    { name: "lokasi4" },
  ];

  return (
    <div className="box-border border-black border-1 rounded-lg">
      <div className="box-content px-5 py-9">
        <h1 className="xs:text-2xl sm:text-3xl lg:text-2xl font-bold pb-3">
          Filter Pencarian
        </h1>
        <div className="grid grid-cols-1 gap-4">
          <Grid item>
            <TextField
              placeholder="Masukkan Nama"
              label="nama"
              labelClass="text-lg"
              type="text"
              name="username"
            />
          </Grid>
          <Grid item>
            <ListBox
              placeholder="Pilih Partner"
              label="Partner"
              listOption={partner}
              labelClass="text-lg"
            />
          </Grid>
          <Grid item>
            <ListBox
              placeholder="Pilih Category"
              label="Kategori"
              listOption={dummyCategory}
              labelClass="text-lg"
            />
          </Grid>
          <Grid item>
            <ListBox
              placeholder="Pilih Lokasi"
              label="Lokasi"
              listOption={dummyLokasi}
              labelClass="text-lg"
            />
          </Grid>
          <Grid item>
            <InputDatePicker
              placeholder="Pilih Tanggal Lahir"
              label="Tanggal Lahir"
              labelClass="text-lg"
            />
          </Grid>
          <Grid item>
            <label className="text-lg mb-3">Rentang Harga</label>
            <InputSlider />
          </Grid>
          <Grid item>
            <label className="text-lg mb-3">Rating</label>
            <InputRating />
          </Grid>
          <Grid item>
            <Button
              onClick={() => handleLink("#")}
              color="primary-bg"
              text="Cari"
              type="submit"
            />
          </Grid>
        </div>
      </div>
    </div>
  );
}
