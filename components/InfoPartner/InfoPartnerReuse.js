import React from "react";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  paper: {
    width: 400,
    height: 400,
    borderRadius: 20,
    border: "5px solid #F6921E",
    backgroundColor: "white",
    backgroundImage: 'url("/img/cute-girl.jpeg")',
    backgroundSize: "cover",
  },
  rowDesc: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
}));

export default function InfoPartnerReuse({ data }) {
  const classes = useStyles();

  return (
    <div 
    className="flex flex-col"
    >
      <div className="w-72 xl:w-90">
      <div
        className="h-72 xl:h-90 rounded-2xl bg-primary-gray"
        style={{
          backgroundImage: `url(${data.profilePicture || data.programBanner})`,backgroundSize:"cover",
        }}
      ></div>
      <div className={classes.columnCard}>
        <div className={classes.rowDesc} style={{ justifyContent: "center" }}>
          <h3>{data.job} - {data.speciality} - {data.subgenre}</h3>
        </div>
        <div className={classes.rowDesc}>
          <div>{data.location}</div>
          <div>Rp {data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")},00</div>
        </div>
      </div>
    </div>
    </div>
  );
}
