import NumberFormat from "react-number-format";

const PriceFormat = ({ className, value, negative=false }) => {
  return (
    <NumberFormat
      value={value}
      displayType={"text"}
      thousandSeparator={true}
      prefix={`${negative ? "-" : ""} Rp `}
      suffix={`,00`}
      renderText={(value, props) => <p className={className} {...props}>{value}</p>}
    />
  );
};

export default PriceFormat;
