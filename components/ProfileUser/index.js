import React from "react";
import Tab from "@/components/TabProfile";
import Banner from "@/components/Banner";
import Ulasan from "./Ulasan";

export default function index() {
  return (
    <>
      <Banner imgSrc="/banner-profile.png" title="David Gadgetin" />
      <Tab />
      <Ulasan />
    </>
  );
}
