import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import Image from "next/image";
import Button from "@/components/Button";

const ModalSuccess = (props) => {
  return (
    <>
      <Transition appear show={props.show} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          open={props.isOpen}
          onClose={props.closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full h-full max-w-[300px] max-h-[402px] py-10 px-10 overflow-hidden text-center align-middle transition-all transform bg-white shadow-xl rounded-[15px]">
                <div className="mt-2">
                  <Image
                    src="/success.png"
                    width="102"
                    height="102"
                    objectFit="cover"
                  />
                  <p className="text-xl font-bold mt-12 text-[#27AE60]">
                    Sukses
                  </p>
                  <p className="text-sm mt-3">
                    Ulasan anda berhasil dikirimkan
                  </p>
                </div>

                <div className="mt-9">
                  <Button
                    type="button"
                    className="inline-flex justify-center px-4 py-4 text-base bg-primary-orange text-white"
                    onClick={props.closeModal}
                  >
                    Ke Profil
                  </Button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default ModalSuccess;
