import { useState } from "react";
import Rating from "@material-ui/lab/Rating";

const RatingProfile = () => {
  const [value, setValue] = useState(0);
  return (
    <div className="flex">
      <Rating
        name="simple-controlled"
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </div>
  );
};

export default RatingProfile;
