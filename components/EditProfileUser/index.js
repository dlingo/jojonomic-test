import ContainerNavigation from "@/components/TabProfile/ContainerNavigation";
import TabList from "@/components/TabProfile/TabList";
import Banner from "@/components/Banner";
import Tab from "./tab";

export default function EditProfile() {
  return (
    <>
      <Banner imgSrc="/banner-profile.png" title="David Gadgetin" />
      <ContainerNavigation>
        <TabList
          label="Pesanan Saya"
          src="/pictprofile.png"
          nama="David Gadgetin"
        />
      </ContainerNavigation>
      <div className="container-content">
        <Tab />
      </div>
    </>
  );
}
