import React, { Fragment, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { Controller } from "react-hook-form";
import dayjs from "dayjs";
import { ErrorMessage } from "@hookform/error-message";

function InputDatePicker({
  label = "test",
  containerClass,
  defaultValues,
  labelClass,
  statusError,
  placeholder,
  required,
  control,
  error,
  name,
}) {
  const [selectedDate, setSelectedDate] = useState(defaultValues ? new Date(defaultValues) : "");

  return (
    <div
      className={`space-y-1 text-xs ${containerClass} flex flex-col relative`}
    >
      {label && (
        <label className={`${labelClass}`}>
          {label}
          {required && <span className="text-red-500 font-bold">*</span>}
        </label>
      )}
      <div
        className={`border-1 p-2 rounded-lg ${
          statusError ? "border-red-500" : "border-gray-900"
        } w-full focus:outline-none focus:border-primary-orange flex items-center`}
      >
        {control ? (
          <Controller
            control={control}
            defaultValue={defaultValues}
            name={name}
            render={({ field: { onChange } }) => (
              <DatePicker
                className="bg-transparent outline-none w-full"
                placeholderText={placeholder}
                selected={selectedDate}
                onChange={(date) => {
                  console.log(date)
                  onChange(dayjs(date).format("YYYY-MM-DD"));
                  setSelectedDate(date);
                }}
                closeOnScroll={true}
              />
            )}
          />
        ) : (
          <DatePicker
            className="bg-transparent outline-none w-full"
            placeholderText={placeholder}
            selected={selectedDate}
            onChange={(date) => setSelectedDate(date)}
            closeOnScroll={true}
          />
        )}
        <ChevronDownIcon style={{ height: 14 }} />
      </div>
      {error && (
        <div className="absolute -bottom-5 text-[11px] text-red-500 line-clamp-1">
          <ErrorMessage
            errors={error}
            name={name}
            render={({ message }) => <p>{message}</p>}
          />
        </div>
      )}
    </div>
  );
}

export default InputDatePicker;
