import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

export default function ModalSearch() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Open dialog
      </Button>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                </DialogTitle>
                <DialogContent dividers>
                    <Grid container spacing={3}>
                        <Grid className="ModalTitle-search" item sm>
                            Apa yang Anda Cari ?
                        </Grid>
                    </Grid>

                    <Grid container direction="column"
                        justify="flex-start"
                        alignContent="flex-start"
                        className="FormStyle"
                    >
                        <form noValidate autoComplete="off" className="FormStyle">
                            <Typography gutterBottom>Nama</Typography>
                            <TextField className="widthInput MuiOutlinedInput-root" id="outlined-basic" variant="outlined" size="small" />
                        </form>
                        <form noValidate autoComplete="off" className="FormStyle">
                            <Typography gutterBottom>Genre/Kategori</Typography>
                            <TextField className="widthInput" id="outlined-basic" variant="outlined" size="small" />
                        </form>
                        <form noValidate autoComplete="off" className="FormStyle">
                            <Typography gutterBottom>Lokasi Acara</Typography>
                            <TextField className="widthInput" id="outlined-basic" variant="outlined" size="small" />
                        </form>
                        <form noValidate autoComplete="off" className="FormStyle widthInput">
                            <Typography gutterBottom>Tanggal Acara</Typography>
                            <form noValidate className="dateStyle MuiInput-underline">
                                <TextField
                                    id="date"
                                    type="date"
                                    className="widthInput"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </form>
                        </form>
                        <Button variant="contained" classname="MuiButton-contained">
                            Cari
                        </Button>
                    </Grid>
                </DialogContent>
            </Dialog>
            <style jsx global>
                {`
                    .customCard-buy-button{
                      background-color: #FF5222;
                      color:white;
                      width:100%;
                    }
                    .MuiDialog-paperWidthSm{
                        border-radius: 20px !important;
                    }
                    .MuiDialogContent-dividers{
                        border: 0 !important;
                        margin-right: 20px;
                        margin-left: 20px;
                    }
                    .MuiGrid-container{
                        justify-content: space-between;
                        align-items: center;
                    }
                    .ModalTitle-search{
                        font-size: 23px;
                        font-weight: 600;
                        font-familiy: montserract;
                        text-align: center;
                    }
                    .FormStyle{
                        margin-top: 10px;
                    }
                    .widthInput{
                        width: 100%;
                    }
                    .dateStyle{
                        border: 1px solid #c7bfbf;
                        border-radius: 10px;
                        padding: 3px;
                    }
                    .MuiOutlinedInput-root{
                        border-radius: 10px !important;
                    }
                    .MuiButton-contained{
                        background-color: #F6921E !important;
                        width: 100% !important;
                        margin-top: 20px !important;
                        margin-bottom: 30px !important;
                        color: #ffffff !important;
                        border-radius: 10px !important;
                    }
                    .MuiInput-underline:before{
                        border-bottom: 0 !important;
                    }
                  `}
            </style>
        </div >
    );
}
