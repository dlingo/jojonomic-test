import { useState, useEffect } from "react";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import { FbIcon, GoogleIcon, CircleSpinIcon } from "@/components/Icons";
import Button from "components/Button";
import { Dialog, Grid, Box, IconButton } from "@material-ui/core";
import TextField from "components/TextField";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import Fade from "react-reveal/Fade";
import {
  loadingStatus,
  errorStatus,
  successStatus,
  completedStatus,
} from "src/redux/loader/action";
import { apiPost } from "src/utils/fetch";
import { GoogleLogin } from "react-google-login";
import { FacebookProvider, Login } from "react-facebook";

const CONSTANT_TYPE = "login";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: "#000000",
  },
  root: {
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "black",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
        borderWidth: 1,
      },
      "&:hover fieldset": {
        borderColor: "#F6921E",
        borderWidth: 1,
      },
    },
  },
  paperWidthSm: {
    maxWidth: 525,
    borderRadius: 20,
    margin: 0,
  },
}));

const DialogTitle = (props) => {
  const classes = useStyles();
  const { children, onClose, ...other } = props;
  return (
    <div
      className="xs:px-5 sm:px-5 md:px-9 lg:px-14 xl:px-16 px-3 pt-10 pb-5"
      {...other}
    >
      <Box>{children}</Box>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </div>
  );
};

export default function ModalLogin(props) {
  const { loader } = useSelector((state) => state);
  const dispatch = useDispatch();
  const classes = useStyles();
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onBlur" });

  const onSubmit = (body) => {
    dispatch(loadingStatus(CONSTANT_TYPE));
    apiPost("/auth/login", body)
      .then((res) => dispatch(successStatus(CONSTANT_TYPE)))
      .catch((err) =>
        dispatch(errorStatus(CONSTANT_TYPE, err?.err?.response?.data?.message))
      );
  };

  useEffect(() => {
    return () => {
      dispatch(completedStatus(CONSTANT_TYPE));
    };
  }, []);

  const responseGoogle = (response) => {
    console.log("response google", response);
  };
  const responseFacebook = (response) => {
    console.log("response facebook", response);
  };

  return (
    <Dialog
      fullWidth
      open={props.openModal}
      onClose={props.handleClickCloseModal}
      aria-labelledby="customized-dialog-title"
      classes={{
        paperWidthSm: classes.paperWidthSm,
      }}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={props.handleClickCloseModal}
      >
        <Box mt={4}>
          <Grid container justify="space-between" alignItems="center">
            <Grid item>
              <div
                className="font-bold xs:text-xl sm:text-xl md:text-xl lg:text-2xl xl:text-3xl text-xl"
                style={{ fontFamily: "poppins" }}
              >
                Masuk
              </div>
            </Grid>
            <Grid item>
              <Link href="/signup">
                <a
                  className="xs:text-sm sm:text-sm md:text-md lg:text-lg text-sm text-primary-orange"
                  style={{ fontFamily: "poppins" }}
                >
                  Daftar
                </a>
              </Link>
            </Grid>
          </Grid>
        </Box>
      </DialogTitle>
      <div className="xs:px-5 sm:px-5 md:px-9 lg:px-14 xl:px-16 px-3 pb-20">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="container mx-auto">
            <div className="mb-6">
              <div className="mb-5 relative">
                <TextField
                  placeholder="Masukkan Nama Pengguna"
                  label="Nama Pengguna"
                  type="text"
                  name="username"
                  error={errors}
                  register={register}
                  validationError={{ required: "Wajib diisi" }}
                  required
                />
              </div>
              <div className="mb-4 relative">
                <TextField
                  placeholder="Masukkan Kata Sandi"
                  label="Kata Sandi"
                  type="password"
                  name="password"
                  error={errors}
                  register={register}
                  validationError={{ required: "Wajib diisi" }}
                  required
                />
              </div>
              {loader?.[CONSTANT_TYPE]?.error && (
                <Fade when={loader?.[CONSTANT_TYPE]?.error}>
                  {" "}
                  <div className="text-center text-red-500 text-xs">
                    {loader?.[CONSTANT_TYPE]?.message}
                  </div>
                </Fade>
              )}
            </div>
            <div
              className="text-right text-primary-orange mb-4"
              style={{ fontFamily: "poppins" }}
            >
              Lupa Kata Sandi?
            </div>
            <div className="mb-2">
              <Button
                disabled={loader?.[CONSTANT_TYPE]?.loading}
                loadingIcon={<CircleSpinIcon className="w-5" />}
                type="submit"
                onClick={handleSubmit(onSubmit)}
                color="primary-bg"
                text="Masuk"
              />
            </div>
            <div
              className="mb-3 text-center text-primary-gray text-md"
              style={{ fontFamily: "poppins" }}
            >
              Masuk Sebagai User
            </div>
            <FacebookProvider appId="537854030851263">
              <Login
                scope="email"
                onCompleted={responseFacebook}
                // onError={handleError}
              >
                {({ loading, handleClick, error, data }) => (
                  <div className="mb-5">
                    <Button
                      onClick={handleClick}
                      color="blue"
                      text="Sign Up with Facebook"
                      name="password"
                      iconPrefix={<FbIcon className="w-5 text-white" />}
                    />
                  </div>
                )}
              </Login>
            </FacebookProvider>
            <GoogleLogin
              clientId="198685548545-li8lpsavrkp4oki5lj2jh781t8g8504o.apps.googleusercontent.com"
              render={(renderProps) => (
                <div>
                  <Button
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    color="gmail"
                    text="Sign Up with Google"
                    iconPrefix={<GoogleIcon className="w-5" />}
                  />
                </div>
              )}
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              // cookiePolicy={"single_host_origin"}
            />
          </div>
        </form>
      </div>
    </Dialog>
  );
}
