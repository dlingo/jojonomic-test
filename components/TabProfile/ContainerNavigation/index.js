import React, { useState } from "react";
import { Tab } from "@headlessui/react";

export default function index({ children }) {
  const [active, setActive] = useState(0);

  const handleChange = (event, newValue) => {
    setActive(newValue);
  };

  return (
    <div className="container-fuild w-full md:h-[80px] lg:h-[126px] bg-primary-gray-feed">
      <Tab.Group onChange={handleChange}>
        <Tab.List className="h-full">{children}</Tab.List>
      </Tab.Group>
    </div>
  );
}
