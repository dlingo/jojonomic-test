import React from "react";
import ContainerNavigation from "./ContainerNavigation";
import TabList from "./TabList";

export default function index() {
  return (
    <>
      <ContainerNavigation>
        <TabList
          label="Pesanan Saya"
          src="/pictprofile.png"
          nama="David Gadgetin"
        />
      </ContainerNavigation>
    </>
  );
}
