import React, { useState } from "react";

const Step = ({ first, index, isLast, completed, active, title, numbered }) => {
  return (
    <>
      <div className="step-wrapper flex items-center text-teal-600 relative">
        <div
          className={`flex items-center justify-center rounded-full transition duration-500 ease-in-out h-7 w-7  ${
            completed || (isLast && active)
              ? "border-2 border-primary-orange"
              : active
              ? "border-2 border-primary-orange"
              : "border-2 border-primary-orange"
          }`}
        >
          <p
            className={`w-5 h-5 rounded-full
            ${
              completed || active
                ? "bg-primary-orange"
                : " bg-transparent opacity-90"
            }
          `}
          />
          {numbered && (
            <div
              className={`w-5 h-5 rounded-full border-2 border-primary-orange absolute`}
            >
              <div
                className={`w-full h-full ${
                  completed || active ? "text-white" : "text-primary-orange"
                } text-14-responsive font-bold flex items-center justify-center`}
              >
                {index}
              </div>
            </div>
          )}
        </div>
        <div
          className={`absolute top-0 text-center mt-12 text-xs left-1/2 transform -translate-x-1/2  text-teal-600 text-blue-primary ${
            active ? "font-bold" : ""
          }`}
        >
          {title}
        </div>
      </div>
      {completed && (
        <div className="flex-auto border-t-2 transition duration-500 ease-in-out border-primary-orange"></div>
      )}
      {!completed && !isLast && (
        <div className="flex-auto border-t-2 transition duration-500 ease-in-out border-gray-500 opacity-30"></div>
      )}
    </>
  );
};

export default React.memo(Step);
