export default function ErrorMessage({message}) {
  if(!message) {
    return null
  }
  return (
    <div className="text-[11px] flex items-center justify-center bg-red-200 text-red-500 p-2 my-2 font-semibold">
      <span>{message}</span>
    </div>
  );
}
