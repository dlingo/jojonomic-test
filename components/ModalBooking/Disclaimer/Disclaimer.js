import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Typography from "@material-ui/core/Typography";

var CustomCheckbox = (props) => {
  const [checked, setChecked] = React.useState(false);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  return (
    <div>
      <Checkbox
        checked={checked}
        onChange={handleChange}
        color={props.color}
        style={
          {
            //   color: "#F6921E",
          }
        }
      />
      {/* <Typography>Catatan untuk Partner</Typography> */}
    </div>
  );
};

export default function Disclaimer() {
  return (
    <FormControl component="fieldset">
      <FormGroup aria-label="position" row>
        <FormControlLabel
          value="end"
          control={<CustomCheckbox color="primary" />}
          labelPlacement="end"
          label={
            <Typography style={{ fontSize: 12 }}>
              Saya telah membaca, memahami, dan menyetujui atas persyaratan dan
              kondisi untuk melakukan pemesanan
            </Typography>
          }
        />
      </FormGroup>
    </FormControl>
  );
}
