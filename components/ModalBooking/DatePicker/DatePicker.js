import "date-fns";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles((theme) => ({
  borderDate: {
    marginTop: 4,
    width: 242,
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        // width: "27ch",
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
}));

export default function DatePicker() {
  const classes = useStyles();

  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        className={classes.borderDate}
        format="dd/MM/yyyy"
        margin="normal"
        size="small"
        value={selectedDate}
        onChange={handleDateChange}
        inputVariant="outlined"
        fullWidth
      />
    </MuiPickersUtilsProvider>
  );
}
