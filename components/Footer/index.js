import { Grid } from "@material-ui/core/";
import Image from "../Image";
import {
  FacebookIcon,
  InstagramIcon,
  YoutubeIcon,
  TwitterIcon,
  AppStoreIcon,
  PlaystoreAppIcon,
} from "@/components/Icons";
import { ChevronRightIcon } from "@heroicons/react/solid";

const perusahaan = [
  { name: "Tentang Kami", link: "#" },
  { name: "Karir", link: "#" },
  { name: "Terms of Use", link: "#" },
  { name: "Privacy Statement", link: "#" },
  { name: "Partner With Us", link: "#" },
];
const layanan = [
  { name: "Creative & Digital", link: "#" },
  { name: "Entertainment", link: "#" },
  { name: "Activation", link: "#" },
  { name: "Technology", link: "#" },
];
const headOffice =
  "Nifarro Park, ITS Tower Jl. Raya Pasar Minggu Jakarta Selatan, DKI Jakarta, Indonesia - 12510 ";

const Footer = () => {
  return (
    <footer className="">
      <div className="container-fluid bg-primary-orange footer-com-section w-full text-center relative hidden md:flex text-xs md:text-base lg:text-lg ">
        <div className="absolute inset-y-0 w-1/2 z-0 bg-primary-white right-0" />
        <div className="w-1/4 py-8 px-2 flex items-center leading-[0] relative">
          <div className="absolute inset-0 bg-primary-white z-0 border-t border-primary-orange" />
          <div className="absolute inset-0 bg-primary-orange z-0 flex items-center justify-center rounded-tr-[2.75rem]">
            <h1 className="text-[1.2em] font-bold text-white">
              Sister Company
            </h1>
          </div>
        </div>
        <div className="flex items-center justify-center w-1/4 py-8 px-2 bg-primary-white border-t border-r border-primary-orange relative z-[1]">
          <div>
            <h1 className="text-[1.2em] font-semibold">ACTIVATION</h1>{" "}
            <h4 className="text-[0.7em] text-primary-orange">Kunjungi Kami</h4>
          </div>
        </div>
        <div className="flex items-center justify-center w-1/4 py-8 px-2 bg-primary-white border-t border-r border-primary-orange relative z-[1]">
          <div>
            <h1 className="text-[1.2em] font-semibold">Benang Merah</h1>
            <h4 className="text-[0.7em] text-primary-orange">Kunjungi Kami</h4>
          </div>
        </div>
        <div className="flex items-center justify-center w-1/4 py-8 px-2 bg-primary-white border-t border-r border-primary-orange rounded-tr-[2.75rem] relative z-[1]">
          <div>
            <h1 className="text-[1.2em] font-semibold">V ENTERTAINMENT</h1>
            <h4 className="text-[0.7em] text-primary-orange">Kunjungi Kami</h4>
          </div>
        </div>
      </div>
      <div className="container-fluid py-6 bg-gradient-to-r from-[#F6921E] to-[#FFB500] text-xs text-white">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={3} className="space-y-4">
            <a href="/">
              <Image
                layout="fixed"
                width={155}
                height={50}
                src="https://eventori.id/assets/images/logo/logo-putih.png"
                alt="logo"
                bgTransparent
              />
            </a>
            <p>
              Eventori adalah pilihan utamamu untuk ecommerce dunia hiburan.
              Eventori hadir untuk merayakan momen special di hidupmu!
            </p>
            <div className="flex space-x-3">
              <InstagramIcon className="w-4 text-white" />
              <FacebookIcon className="w-2.5 text-white" />
              <TwitterIcon className="w-5 text-white" />
              <YoutubeIcon className="w-5 text-white" />
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={3} className="">
            <h2 className="text-lg font-bold mb-4">Perusahaan</h2>
            <ul className="space-y-2">
              {perusahaan.map((item, index) => {
                return (
                  <li key={index} className="flex items-center space-x-1">
                    <ChevronRightIcon className="w-4 text-white" />{" "}
                    <a href={item.link}>{item.name}</a>
                  </li>
                );
              })}
            </ul>
          </Grid>
          <Grid item xs={12} sm={6} md={3} className="">
            <h2 className="text-lg font-bold mb-4">Layanan Lainnya</h2>
            <ul className="space-y-2">
              {layanan.map((item, index) => {
                return (
                  <li key={index} className="flex items-center space-x-1">
                    <ChevronRightIcon className="w-4 text-white" />{" "}
                    <a href={item.link}>{item.name}</a>
                  </li>
                );
              })}
            </ul>
          </Grid>
          <Grid item xs={12} sm={6} md={3} className="">
            <h2 className="text-lg font-bold mb-4">Head Office</h2>
            <p>{headOffice}</p>
            <div className="flex space-x-3 mt-4 mb-6">
              <PlaystoreAppIcon className="w-[7.8rem]" />
              <AppStoreIcon className="w-28" />
            </div>
          </Grid>
        </Grid>
        <div className="py-3">
          <p>Copyright © Eventori. All Rights Reserved.</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
