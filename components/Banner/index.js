import { upperCase } from "lodash";
import Image from "next/image";

const Banner = ({ imgSrc, title, slug="", type, partner }) => {
    const basePath = "home/";
    const path = basePath.concat(slug.split("/").splice(1, 2).join("/"));
    const pathStyle = upperCase(path).replaceAll(" ", " / ");

    const partnerTitle = upperCase(slug.split("/").splice(-1, 1));

  return (
    <div className="mt-[64px] md:mt-0 w-screen">
      <div className="relative">
        <Image
          src={imgSrc}
          width={1440}
          height={430}
          alt="banner"
          layout="responsive"
          objectFit="cover"
          className="absolute inset-0"
        />
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="md:mt-[calc(6.25%)] text-center">
            {slug && (
              <div className="text-[10px] md:text-xs lg:text-sm xl:text-lg font-bold text-white">
                {/* HOME / PARTNER / {upperCase(partner)} / */}
                {pathStyle} /
                <span className="text-primary-orange"> {upperCase(type)} </span>
              </div>
            )}
            <h1
              className={`${
                slug && "mt-1 lg:mt-2"
              } font-pop text-sm md:text-2xl lg:text-[30px] xl:text-4xl font-bold text-white`}
            >
              {title || partnerTitle}
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
