import * as React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  colorPrimary: {
    color: "#FFAC33",
  },
  borderDate: {
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
}));

export default function InputSlider() {
  const classes = useStyles();
  const [value, setValue] = React.useState([0, 100]);

  const handleInputChange = (event, type) => {
    if (type === "min") {
      return setValue([
        event.target.value === "" ? "" : Number(event.target.value),
        value[1],
      ]);
    }
    setValue([
      value[0],
      event.target.value === "" ? "" : Number(event.target.value),
    ]);
  };

  const handleBlur = () => {
    if (value < 0) {
      setValue(0);
    }
  };

  const handleChange = (event, newValue) => {
    return setValue(newValue);
  };

  return (
    <div>
      <div className="grid sm:grid-cols-1 lg:grid-cols-2 gap-3">
        <div>
          <label>minimal</label>
          <TextField
            className={classes.borderDate}
            variant="outlined"
            value={value[0]}
            size="small"
            fullWidth
            onChange={(e) => handleInputChange(e, "min")}
            onBlur={handleBlur}
            inputProps={{
              step: 10,
              min: 0,
              max: 100,
              type: "number",
              "aria-labelledby": "input-slider",
            }}
          />
        </div>
        <div>
          <label>maksimal</label>
          <TextField
            className={classes.borderDate}
            variant="outlined"
            value={value[1]}
            size="small"
            fullWidth
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
              step: 10,
              min: 0,
              max: 100,
              type: "number",
              "aria-labelledby": "input-slider",
            }}
          />
        </div>
      </div>
      <div className="grid grid-cols-1">
        <Slider
          classes={{ colorPrimary: classes.colorPrimary }}
          value={value}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}
