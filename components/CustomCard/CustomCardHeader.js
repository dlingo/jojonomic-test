import Link from "next/link";
const CustomCardHeader = ({ title, readMore, type }) => {
  return (
    <div className="font-bold" style={{ fontFamily: "Poppins" }}>
      <div className="flex justify-between items-end mb-5 md:mb-4 xl:mb-8">
        <h1 className="leading-tight sm:leading-normal text-lg md:text-2xl xl:text-4xl">
          {title}
        </h1>
        <Link href={`/partner/${type}`}>
          <h3 className="text-[8px] md:text-sm xl:text-lg text-primary-orange cursor-pointer">
            Lihat Semua {readMore}
          </h3>
        </Link>
      </div>
    </div>
  );
};

export default CustomCardHeader;
