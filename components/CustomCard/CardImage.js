import Image from "next/image";

const CardImage = ({
  type,
  imageSrc = "https://images.unsplash.com/photo-1598342473022-5ced5bb5c291?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80",
  alt = "test",
}) => {
  return (
    <>
      {type == "square" ? (
        <Image
          src={imageSrc}
          width={360}
          height={360}
          alt={`${alt} Profile Picture`}
          layout="responsive"
          objectFit="cover"
          className="absolute inset-0 rounded-2xl cursor-pointer"
        />
      ) : type == "landscape" ? (
        <Image
          src={imageSrc}
          width={270}
          height={160}
          alt={`${alt} Product Picture`}
          layout="responsive"
          objectFit="cover"
          className="rounded-2xl cursor-pointer"
        />
      ) : (
        type == "detail" && (
          <Image
            src={imageSrc}
            width={471}
            height={382}
            alt={`${alt} Profile Picture`}
            layout="responsive"
            objectFit="cover"
            className="absolute inset-0 rounded-2xl"
          />
        )
      )}
    </>
  );
};

export default CardImage;
