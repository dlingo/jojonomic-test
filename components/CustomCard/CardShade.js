const CardShade = ({ opacity = "30", link }) => {
  return (
    <div
      className={`absolute inset-0 bg-black opacity-${opacity} rounded-2xl ${
        link ? "cursor-pointer" : ""
      }`}
    ></div>
  );
};

export default CardShade;
