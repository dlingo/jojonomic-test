import { Icon } from "@iconify/react";

const BoxCategory = ({ category }) => {
  const iconProgram = (category) => {
    switch (category) {
      case "event":
        return "mdi:party-popper";
      case "musik":
        return "bi:music-note";
      default:
        return "akar-icons:info";
    }
  };

  return (
    <div className=" max-w-content px-[.8em] py-[.3em] bg-primary-purple absolute top-0 left-[21px]">
      <div className="flex justify-center items-center">
        <Icon icon={iconProgram(category)} width="14" color="white" />
        <p className="text-[10px] md:text-xs xl:text-base text-white capitalize ml-[5px]">
          {category}
        </p>
      </div>
    </div>
  );
};

export default BoxCategory;
