import CardImage from "./CardImage";
import BoxCategory from "./BoxCategory";
import PromotedOverlay from "./PromotedOverlay";
import CardInfo from "./CardInfo";
import CardShade from "./CardShade";
import Bookmark from "./Bookmark";
import { BookmarkIcon } from "@/components/Icons";

const CustomCard = ({
  type = "plain",
  // partner, program, detail, plain
  category,
  // talent, influencer, venue, vendor, program
  data,
  src,
  shade = 20,
}) => {
  const handleStyle = () => {
    if (type != "partner") {
      if (type == "detail") {
        return {
          cardImageType: "detail",
        };
      } else {
        return {
          cardImageType: "square",
        };
      }
    }

    if (category == "talent" || category == "influencer") {
      return {
        cardImageType: "square",
        promotedType: "large",
      };
    }

    if (category == "venue" || category == "vendor") {
      return {
        cardImageType: "landscape",
        promotedType: "small",
      };
    }
  };

  return (
    <div className="flex flex-col items-center shadow-lg rounded-lg">
      <div className="w-full">
        {/* Image */}
        <div className="relative">
          <CardImage
            type={handleStyle().cardImageType}
            imageSrc={src || data.profilePicture || data.programBanner}
            alt={data}
          />
          {/* {shade && type != "detail" ? (
            <CardShade opacity={shade} link />
          ) : (
            <CardShade opacity={shade} />
          )} */}
          {type == "detail" && <Bookmark />}
          {/* {data.promoted && (
            <PromotedOverlay type={handleStyle().promotedType} />
          )} */}
          {type == "program" && (
            <>
              <BoxCategory category={data} />
            </>
          )}
        </div>

        {/* Desc */}
        {type == "partner" && <CardInfo category={category} data={data} />}
      </div>
    </div>
  );
};

export default CustomCard;
