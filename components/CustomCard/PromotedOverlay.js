import { PromotedIcon } from "../Icons";

const PromotedOverlay = ({
  type = "large",
  // talent/influencer dan venue/vendor
}) => {
  return (
    <>
      <div className="absolute inset-0 bg-black opacity-30 rounded-2xl cursor-pointer"></div>
      <div className="absolute left-[1.6em] bottom-[.8em] xl:left-[2em] xl:bottom-[1.1em] opacity-70">
        <div className="flex items-center">
          {type == "large" ? (
            <>
              <PromotedIcon className="w-4 lg:w-[20px] xl:w-[24px] text-white" />
              <p className="text-xs lg:text-base xl:text-lg text-white ml-[.67em]">
                Promoted
              </p>
            </>
          ) : (
            type == "small" && (
              <>
                <PromotedIcon className="w-[8px] md:w-[10px] text-white" />
                <p className="text-[10px] md:text-xs text-white ml-[.67em]">
                  Promoted
                </p>
              </>
            )
          )}
        </div>
      </div>
    </>
  );
};

export default PromotedOverlay;
