import React from "react";
import Button from "components/Button";
import Image from "components/Image";
import ModalGaleri from "../ModalGaleri";

const CardGaleri = () => {
  const [openModal, setOpen] = React.useState(false);
  const handleClickOpenModal = () => {
    setOpen(true);
  };
  const handleClickCloseModal = () => {
    setOpen(false);
  };
  return (
    <div className="w-full h-full m-auto bg-black rounded-lg overflow-hidden shadow-lg">
      <Image src="/carousel1.png" className="rounded-t-lg object-cover" />
      <div className="xs:px-2 xs:pb-2 sm:px-4 lg:px-6 pt-2 pb-1">
        <div className="font-bold xs:text-[10px] sm:text-lg lg:text-lg text-white">
          Ruangan Komputer
        </div>
      </div>
      <div className="grid grid-cols-2 xs:px-2 sm:px-4 lg:px-6 mb-3">
        <p className="text-white xs:pt-[7px] pt-2 lg:pt-3 xs:text-[8px] sm:text-lg lg:text-[16px]">
          Rp 12.000.000
        </p>
        <div className="text-right">
          <Button
            text="Detail"
            onClick={handleClickOpenModal}
            classText="text-white xs:text-[8px] sm:text-lg lg:text-[16px] font-bold"
            className="xs:p-0 xs:w-[50px] sm:w-[100px] sm:mt-2 bg-primary-orange border-transparent"
          />
          <ModalGaleri
            openModal={openModal}
            handleClickCloseModal={handleClickCloseModal}
          />
        </div>
      </div>
    </div>
  );
};

export default CardGaleri;
