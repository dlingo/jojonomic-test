/* This example requires Tailwind CSS v2.0+ */
import React, { Fragment, useState } from "react";
import dynamic from "next/dynamic";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";

const ModalLogin = dynamic(import("../ModalLogin/ModalLogin"));

import DataAppbar from "../../data/DataAppbar.json";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function AppBar() {
  const [openModal, setOpen] = React.useState(false);
  const handleClickOpenModal = () => {
    setOpen(true);
  };
  const handleClickCloseModal = () => {
    setOpen(false);
  };
  return (
    <Disclosure
      as="nav"
      className="xs:bg-primary-black-responsive md:bg-primary-black-transparent lg:bg-primary-black-transparent w-screen fixed z-10"
    >
      {({ open }) => (
        <>
          {openModal && (
            <ModalLogin
              openModal={openModal}
              handleClickCloseModal={handleClickCloseModal}
            />
          )}
          <div className="sm:px-6 lg:py-4 xl:py-7 lg:mx-auto lg:px-8">
            <div className="relative flex items-center justify-between h-16 md:visible lg:visible">
              <div className="absolute inset-y-0 right-0 flex items-center lg:hidden">
                {/* Mobile menu button*/}
                <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XIcon className="block h-6 w-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>
              <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div className="flex-shrink-0 flex items-center">
                  <img
                    className="block lg:hidden h-10 w-auto"
                    src="https://www.eventori.id/assets/images/logo/logo-putih.png"
                    alt="Workflow"
                  />
                  <Link href="/">
                    <img
                      className="hidden lg:block h-11 w-auto cursor-pointer"
                      src="https://www.eventori.id/assets/images/logo/logo-putih.png"
                      alt="Workflow"
                    />
                  </Link>
                </div>
                {/* navigation dropdown dekstop */}
                <div className="hidden lg:block sm:ml-6 lg:m-0 w-full md:hidden">
                  <div className="flex flex-Grow justify-center">
                    {DataAppbar.navigation.map((item, key) => (
                      <Menu key={key} as="div" className="relative">
                        <div>
                          <Menu.Button className="bg-tranparent flex text-sm">
                            <p
                              key={item.name}
                              href={item.href}
                              className={classNames(
                                item.current
                                  ? "text-white"
                                  : "text-white hover:bg-gray-700 hover:text-white",
                                "px-3 py-2 rounded-md text-lg font-bold"
                              )}
                              aria-current={item.current ? "page" : undefined}
                            >
                              {item.name}
                            </p>
                          </Menu.Button>
                        </div>
                        <Transition
                          as={Fragment}
                          enter="transition ease-out duration-100"
                          enterFrom="transform opacity-0 scale-95"
                          enterTo="transform opacity-100 scale-100"
                          leave="transition ease-in duration-75"
                          leaveFrom="transform opacity-100 scale-100"
                          leaveTo="transform opacity-0 scale-95"
                        >
                          <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-primary-black-transparent ring-1 ring-black ring-opacity-5 focus:outline-none">
                            {item.dropdown.map((item2, key) => {
                              return (
                                <Menu.Item key={key}>
                                  {({ active }) => (
                                    <a
                                      href={item2.href}
                                      className={classNames(
                                        active ? "bg-gray-400" : "",
                                        "block px-4 py-2 text-sm text-white"
                                      )}
                                    >
                                      {item2.subtitle}
                                    </a>
                                  )}
                                </Menu.Item>
                              );
                            })}
                          </Menu.Items>
                        </Transition>
                      </Menu>
                    ))}
                  </div>
                </div>
              </div>
              <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0 sm:invisible lg:visible">
                {/* Profile dropdown */}
                {/* <Menu as="div" className="ml-3 relative">
                  <div>
                    <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                      <span className="sr-only">Open user menu</span>
                      <img
                        className="h-8 w-8 rounded-full"
                        src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                        alt=""
                      />
                    </Menu.Button>
                  </div>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                  >
                    <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-primary-black-transparent ring-1 ring-black ring-opacity-5 focus:outline-none">
                      {DataAppbar.profil.map((item, key) => {
                        return (
                          <Menu.Item key={key}>
                            {({ active }) => (
                              <a
                                href="#"
                                className={classNames(
                                  active ? "bg-gray-400" : "",
                                  "block px-4 py-2 text-sm text-white"
                                )}
                              >
                                {item.title}
                              </a>
                            )}
                          </Menu.Item>
                        );
                      })}
                    </Menu.Items>
                  </Transition>
                </Menu>
                <div className="text-white text-lg ml-3">Steve</div> */}
                <div className="hidden sm:none lg:block">
                  <div className="grid grid-cols-2 gap-3">
                    <button className="bg-transparent border-2 rounded-md">
                      <div
                        className="text-white text-sm lg:px-3 xl:px-5py-1 font-bold"
                        style={{ fontFamily: "Poppins" }}
                        onClick={handleClickOpenModal}
                      >
                        Masuk
                      </div>
                    </button>
                    <Link href="/signup">
                      <button className="bg-primary-orange border-2 border-transparent rounded-md">
                        <div
                          className="text-white text-sm xl:px-6 lg:px-3 py-1 font-bold"
                          style={{ fontFamily: "Poppins" }}
                        >
                          Daftar
                        </div>
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Disclosure.Panel>
            <div className="px-2 pt-2 pb-3 space-y-1 h-screen">
              {DataAppbar.navigation.map((item, key) => (
                <div key={key}>
                  <a
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-900 text-white"
                        : "text-gray-300 hover:bg-gray-700 hover:text-white",
                      "block px-3 py-2 rounded-md text-sm font-medium"
                    )}
                    aria-current={item.current ? "page" : undefined}
                  >
                    {item.name}
                  </a>
                </div>
              ))}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}
