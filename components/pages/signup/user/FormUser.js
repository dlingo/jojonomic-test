import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidation } from "./utils";
import { LoadingOverlay } from "@/components/Loading";
import { useSelector, useDispatch } from "react-redux";
import ErrorMessage from "@/components/ErrorMessage";
import { apiPost } from "src/utils/fetch";
import {
  loadingStatus,
  errorStatus,
  successStatus,
  completedStatus,
} from "src/redux/loader/action";

const CONSTANT_TYPE = "register_user";

const defaultValues = {
  name: "bill",
  username: "aas",
  phone_number: "4234234234",
  email: "bluebil1049@hotmail.com",
  password: "12345678",
  confirm_password: "12345678",
};

const User = () => {
  const { loader } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleBack = () => {
    router.back();
  };

  const handleDaftar = (data) => {
    console.log("data: ", data);
    const body = {
      user: {
        address: "null",
        ...data,
        phone_number: String(data["phone_number"]),
      },
    };
    dispatch(loadingStatus(CONSTANT_TYPE));
    apiPost("/auth/register-user", body)
      .then((res) => {
        router.push("/signup/success");
        dispatch(successStatus(CONSTANT_TYPE));
      })
      .catch((err) =>
        dispatch(errorStatus(CONSTANT_TYPE, err?.response?.data?.message))
      );
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidation),
    mode: "onBlur",
    defaultValues,
  });

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8 relative overflow-hidden">
        <LoadingOverlay loading={loader?.[CONSTANT_TYPE]?.loading} />
        <form onSubmit={handleSubmit(handleDaftar)}>
          <div className="space-y-3">
            <div className="grid grid-cols-5  items-center justify-center">
              <a
                onClick={handleBack}
                className="text-[10px] xs:text-xs text-left text-primary-orange font-medium cursor-pointer"
              >
                Kembali
              </a>
              <h1 className="text-base xs:text-xl sm:text-2xl font-bold col-span-3">
                Daftar Sebagai
              </h1>
              <button
                type="submit"
                className="text-[10px] xs:text-xs text-right text-primary-orange font-medium"
              >
                Daftar
              </button>
            </div>
            <p className="font-semibold text-xs sm:text-sm">User</p>
          </div>
          <div className="mt-8 space-y-4 w-full">
            <Grid container spacing={2}>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukan Nama Lengkap"
                  label="Nama Lengkap"
                  required
                  name="name"
                  error={errors}
                  register={register}
                />
              </Grid>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukan Nama Pengguna"
                  label="Nama Pengguna"
                  required
                  name="username"
                  error={errors}
                  register={register}
                />
              </Grid>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukan Email"
                  label="Email"
                  required
                  name="email"
                  error={errors}
                  register={register}
                />
              </Grid>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukan No Telepon"
                  label="Nomor Telepon"
                  prefixPhone
                  required
                  name="phone_number"
                  error={errors}
                  register={register}
                />
              </Grid>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukan Kata Sandi"
                  label="Kata Sandi"
                  required
                  type="password"
                  autoComplete="new-password"
                  name="password"
                  error={errors}
                  register={register}
                />
              </Grid>
              <Grid item xs={12} md={4} className="text-left !mb-4">
                <TextField
                  placeholder="Masukkan Konfirmasi Sandi"
                  label="Konfirmasi Kata Sandi"
                  required
                  type="password"
                  autoComplete="new-password"
                  name="confirm_password"
                  error={errors}
                  register={register}
                />
              </Grid>
            </Grid>
          </div>
          <ErrorMessage message={loader?.[CONSTANT_TYPE]?.message} />
        </form>
      </CardSignup>
    </section>
  );
};

export default User;
