import { useEffect } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

const VendorInfo = dynamic(import("./VendorInfo"));
const VendorAbout = dynamic(import("./VendorAbout"));
const VendorPic = dynamic(import("./VendorDetail"));
const VendorPhoto = dynamic(import("./VendorPhoto"));

export default function Vendor() {
  const router = useRouter();
  const step = router.query?.step;

  if (step === "vendor-info") {
    return <VendorInfo />;
  }
  if (step === "vendor-about") {
    return <VendorAbout />;
  }
  if (step === "vendor-detail") {
    return <VendorPic />;
  }
  if (step === "vendor-photo") {
    return <VendorPhoto />;
  }

  return <div>loading...</div>;
}
