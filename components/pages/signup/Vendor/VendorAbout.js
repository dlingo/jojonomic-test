import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import Button from "components/Button";
import ListBox from "pages/signup/ListBox";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationTwo } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepTwoVendor } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const VendorAbout = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const { data: cities } = useSWR("/reference/cities", fetcher);

  const defaultValues = {
    user: {
      address: partner?.vendor?.stepTwo?.user?.address || "",
    },
    partner: {
      description: partner?.vendor?.stepTwo?.partner?.description || "",
      city_id: partner?.venue?.stepTwo?.partner?.city_id || "",
    },
    list: { ...partner?.venue?.stepTwo?.list },
  };

  const handleLink = (href) => {
    router.push(href);
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationTwo),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepTwoVendor(data));
    handleLink("/signup/vendor?step=vendor-detail");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Vendor" activeStep={2} />
        <form
          onSubmit={handleSubmit(handleStepRegister)}
          className="mt-8 space-y-4 mb-12 w-full"
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={8} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Alamat Vendor"
                label="Alamat Vendor"
                required
                name="user.address"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.address}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Domisili"
                placeholder="Pilih Domisili"
                listOption={cities?.data || []}
                name="partner.city_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.city_id}
                defaultValues={defaultValues?.list?.partner?.city_id}
              />
            </Grid>
            <Grid item xs={12} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Deskripsi Vendor"
                label="Deskripsi Vendor"
                required
                name="partner.description"
                required
                register={register}
                error={errors}
                statusError={errors?.partner?.description}
              />
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default VendorAbout;
