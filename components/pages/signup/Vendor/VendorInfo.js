import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import ListBox from "pages/signup/ListBox";
import { PlusIcon } from "@heroicons/react/solid";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationOne, timeArr } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepOneVendor, stepSocialMedia } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const SocialMediaDialog = dynamic(() => import("../SocialMediaDialog"));

const VendorInfo = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const [openDialog, setOpenDialog] = useState(false);

  const handleLink = (href) => {
    router.push(href);
  };
  const { data: categoriesPartner } = useSWR(
    "/reference/partner_categories",
    fetcher
  );

  const defaultValues = {
    user: {
      name: partner?.vendor?.stepOne?.user?.name || "",
      username: partner?.vendor?.stepOne?.user?.username || "",
      email: partner?.vendor?.stepOne?.user?.email || "",
      password: partner?.vendor?.stepOne?.user?.password || "",
      confirm_password: partner?.vendor?.stepOne?.user?.confirm_password || "",
      phone_number: partner?.vendor?.stepOne?.user?.phone_number || "",
    },
    partner: {
      category_id: partner?.vendor?.stepOne?.partner?.category_id || "",
      city_id: partner?.vendor?.stepOne?.partner?.city_id || "",
    },
    vendor: {
      open: partner?.vendor?.stepOne?.vendor?.open || "",
      close: partner?.vendor?.stepOne?.vendor?.close || "",
    },
    list: { ...partner?.vendor?.stepOne?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationOne),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepOneVendor(data));
    handleLink("/signup/vendor?step=vendor-about");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Vendor" activeStep={1} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Lengkap"
                label="Nama Lengkap"
                name="user.name"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.name}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Pengguna"
                label="Nama Pengguna"
                name="user.username"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.username}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nomor Telepon"
                label="Nomor Telepon"
                name="user.phone_number"
                register={register}
                prefixPhone
                required
                error={errors}
                statusError={errors?.user?.phone_number}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Email"
                label="Email"
                name="user.email"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.email}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Kategori"
                placeholder="Pilih Kategori"
                listOption={categoriesPartner?.data || []}
                name="partner.category_id"
                selectedName="name_categories"
                selectedValue="id"
                setValue={setValue}
                control={control}
                error={errors}
                statusError={errors?.partner?.category_id}
                defaultValues={defaultValues?.list?.partner?.category_id}
                required
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <div className={`space-y-1 text-xs flex flex-col`}>
                <label className="text-xs mb-1">Jam Operasional</label>
                <Grid container spacing={1} className="!mt-0">
                  <Grid item xs={6} className="text-left !py-0">
                    <ListBox
                      placeholder="Dari"
                      listOption={timeArr || []}
                      name="vendor.open"
                      selectedName="name"
                      selectedValue="name"
                      setValue={setValue}
                      control={control}
                      error={errors}
                      statusError={errors?.vendor?.open}
                      defaultValues={defaultValues?.list?.vendor?.open}
                    />
                  </Grid>
                  <Grid item xs={6} className="text-left !py-0">
                    <ListBox
                      placeholder="Sampai"
                      listOption={timeArr || []}
                      name="vendor.close"
                      selectedName="name"
                      selectedValue="name"
                      setValue={setValue}
                      control={control}
                      error={errors}
                      statusError={errors?.vendor?.close}
                      defaultValues={defaultValues?.list?.vendor?.close}
                    />
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Kata Sandi"
                label="Kata Sandi"
                name="user.password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Konfirmasi Sandi"
                label="Konfirmasi Kata Sandi"
                name="user.confirm_password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.confirm_password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <div className={`space-y-1 text-xs flex flex-col`}>
                <label>Akun Sosial Media</label>
                <button
                  onClick={() => setOpenDialog(!openDialog)}
                  type="button"
                  className="justify-center border-1 p-1 rounded-lg border-primary-orange w-full bg-primary-orange focus:outline-none focus:border-primary-orange flex items-center active:opacity-70"
                >
                  <PlusIcon className="text-white w-6" />
                </button>
              </div>
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
      <SocialMediaDialog
        open={openDialog}
        setOpen={setOpenDialog}
        dispatchSocMed={(data) => dispatch(stepSocialMedia(data, "vendor"))}
        listValue={null}
      />
    </section>
  );
};

export default VendorInfo;
