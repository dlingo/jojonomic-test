import React from "react";
import Dialog from "@material-ui/core/Dialog";
import Grid from "@material-ui/core/Grid";
import TextField from "components/TextField";
import Slide from "@material-ui/core/Slide";
import { XIcon, XCircleIcon } from "@heroicons/react/solid";
import ListBox from "pages/signup/ListBox";
import Button from "components/Button";
import { useForm, useFieldArray } from "react-hook-form";
import { useSelector } from "react-redux";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const addValue = {
  account_name: "",
  followers: "",
  socmed_id: "",
};

export default function SocialMediaDialog({ open, setOpen, dispatchSocMed, listValue }) {
  const { signup: partner } = useSelector((state) => state);
  const { data: socmedType } = useSWR("/reference/socmed_type", fetcher);

  const defaultValues = listValue ? { partner_socmeds: listValue} : {
    partner_socmeds: [
      {
        account_name: "",
        followers: "",
        socmed_id: "",
      },
    ],
  };

  const { register, handleSubmit, setValue, control } = useForm({
    defaultValues,
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: "partner_socmeds",
  });

  const handleSubmitSocMed = (data) => {
    dispatchSocMed(data)
    handleClose();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleRemove = (index) => {
    if (index > 0) {
      remove(index);
    }
  };

  return (
    <div className="font-pop relative overflow-scroll">
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        maxWidth="lg"
        className="overflow-scroll modal-social-media"
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className="p-4 font-pop flex-col justify-between max-h-[60vh] min-h-[450px] relative overflow-hidden">
          <div className="flex-1 h-full">
            <div className="flex justify-end w-full">
              <button type="button" onClick={handleClose}>
                <XIcon className="w-8" />
              </button>
            </div>
            <div>
              <h1 className="font-semibold text-lg text-center mb-4">
                Akun Media Social
              </h1>
              <div>
                <form>
                  <div className="overflow-y-scroll pb-[183px] h-[250px]">
                    {fields.map((item, index) => {
                      return (
                        <Grid
                          key={index}
                          container
                          spacing={2}
                          className="relative"
                        >
                          <Grid item xs={12} md={4} className="text-left !mb-4">
                            <ListBox
                              label="Social Media"
                              placeholder="Pilih"
                              listOption={socmedType?.data || []}
                              name={`partner_socmeds.${index}.socmed_id`}
                              selectedName="name"
                              selectedValue="id"
                              setValue={setValue}
                              control={control}
                            />
                          </Grid>
                          <Grid item xs={12} md={4} className="text-left !mb-4">
                            <TextField
                              placeholder="Nama"
                              label="Nama Akun"
                              name={`partner_socmeds.${index}.account_name`}
                              register={register}
                            />
                          </Grid>
                          <Grid item xs={12} md={4} className="text-left !mb-4">
                            <TextField
                              placeholder="Jumlah"
                              label="Jumlah Follower"
                              name={`partner_socmeds.${index}.followers`}
                              register={register}
                            />
                          </Grid>
                          {index > 0 && (
                            <button
                              type="button"
                              className="absolute right-2 top-2"
                              onClick={() => handleRemove(index)}
                            >
                              <XCircleIcon className="w-4 text-red-500" />
                            </button>
                          )}
                        </Grid>
                      );
                    })}
                  </div>
                </form>
                <div>
                  <Button
                    onClick={() => append(addValue)}
                    type="button"
                    className="!rounded-xl"
                    color="primary-bg"
                    text="Tambah Akun Lain"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="w-full">
            <div className="mt-4">
              <Button
                onClick={handleSubmit(handleSubmitSocMed)}
                className="!rounded-xl"
                color="primary-bg"
                text="Selesai"
              />
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
}
