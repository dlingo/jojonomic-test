import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import Button from "components/Button";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationTwo } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepTwoVenue } from "@/redux/signup/action";

const VenueAbout = () => {
  const [openDialog, setOpenDialog] = useState(false);

  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const defaultValues = {
    user: {
      address: partner?.venue?.stepTwo?.user?.address || "",
    },
    partner: {
      description: partner?.venue?.stepTwo?.partner?.description || "",
    },
  };

  const handleLink = (href) => {
    router.push(href);
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationTwo),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepTwoVenue(data));
    handleLink("/signup/venue?step=venue-detail");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Venue" activeStep={2} />
        <form onSubmit={handleSubmit(handleStepRegister)} className="mt-8 space-y-4 mb-12 w-full">
          <Grid container spacing={2}>
            <Grid item xs={12} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Alamat Venue"
                label="Alamat Venue"
                required
                name="user.address"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.address}
              />
            </Grid>
            <Grid item xs={12} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Deskripsi Venue"
                label="Deskripsi Venue"
                required
                name="partner.description"
                required
                register={register}
                error={errors}
                statusError={errors?.partner?.description}
              />
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default VenueAbout;
