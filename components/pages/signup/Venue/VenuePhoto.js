// import Image from "@/components/Image";
import Image from "next/image";
import { useState } from "react";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import { ImagePlaceIcon } from "@/components/Icons";
import TextField from "components/TextField";
import ImageUpload from "../ImageUpload";
import Button from "components/Button";

import { useForm, useFieldArray } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationPhoto } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepPhotoVenue, stepFourVenue } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";

const arrList = Array(8).fill(null);

const addValue = {
  area_size: "",
  capacity: "",
  detail_items: "",
  minimum_order: "",
  name: "",
  price: "",
  quota: "",
  venue_vendor_item_images: [],
};

const TalentPhoto = () => {
  const [images, setImages] = useState(null);
  const [imagesReducer, setImagesReducer] = useState([]);
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const defaultValues = {
    venue_vendor_items: [addValue],
  };

  const {
    control,
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationPhoto),
    mode: "onBlur",
    defaultValues,
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: "venue_vendor_items",
  });

  console.log(errors);
  const handleStepRegister = (data, type) => {
    if (type === "add") {
      return append(addValue);
    }
    dispatch(stepFourVenue(data));
    handleLink("/signup/verification/email?type=venue")
  };

  const handleRemove = (index) => {
    if (fields.length > 1) {
      remove(index);
    }
  };

  const handleImage = (item, type, indexImage, index, itemList) => {
    const method = type === "upload" ? "POST" : "DELETE";
    const formData = new FormData();
    setValue(`venue_vendor_items[${index}].venue_vendor_item_images`, itemList)
    if (type === "upload") {
      formData.append("image", item.file, item.file.name);
      fetcher("/auth/upload-image", { method, data: formData })
        .then((res) => {
          dispatch(stepPhotoVenue(res.data, index));
        })
        .catch((err) => {
          console.log("error");
        });
    } else {
      dispatch(removePhotoInfluencer(index));
      // fetcher("/auth/delete-image", { method, data: item?.filename })
      // .then((res) => {
      //   dispatch(removePhotoInfluencer(index))
      // })
      // .catch((err) => {
      //   console.log("error");
      // });
    }
  };

  console.log(imagesReducer)

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Veneu" activeStep={4} />
        <form className="mt-8 space-y-4 mb-12 w-full">
          <div className="flex justify-between items-center">
            <p className="font-bold">Ruangan Yang Di Sewakan</p>
          </div>
          <div className="divide-y-2 divide-gray-600">
            {fields.map((item, index) => {
              return (
                <Grid key={index} container spacing={2} className="py-4">
                  <Grid item xs={12} md={4} className="text-left !mb-4">
                    <TextField
                      placeholder="Masukkan Nama Ruangan"
                      label="Nama Ruangan/Tempat"
                      required
                      name={`venue_vendor_items.${index}.name`}
                      register={register}
                      error={errors}
                      statusError={errors?.venue_vendor_items?.[index]?.name}
                    />
                  </Grid>
                  <Grid item xs={12} md={2} className="text-left !mb-4">
                    <TextField
                      placeholder="Kuota"
                      label="Kuota"
                      required
                      name={`venue_vendor_items.${index}.quota`}
                      register={register}
                      error={errors}
                      statusError={errors?.venue_vendor_items?.[index]?.quota}
                    />
                  </Grid>
                  <Grid item xs={12} md={3} className="text-left !mb-4">
                    <TextField
                      placeholder="Masukkan Luas Ruangan(m2)"
                      label="Luas Ruangan"
                      required
                      name={`venue_vendor_items.${index}.area_size`}
                      register={register}
                      error={errors}
                      statusError={
                        errors?.venue_vendor_items?.[index]?.area_size
                      }
                    />
                  </Grid>
                  <Grid item xs={12} md={3} className="text-left !mb-4">
                    <TextField
                      placeholder="Masukkan Kapasitas Ruangan"
                      label="Kapasitas Ruangan"
                      required
                      name={`venue_vendor_items.${index}.capacity`}
                      register={register}
                      error={errors}
                      statusError={
                        errors?.venue_vendor_items?.[index]?.capacity
                      }
                    />
                  </Grid>
                  <Grid item xs={12} md={6} className="text-left !mb-4">
                    <TextField
                      placeholder="Masukkan Harga Sewa"
                      label="Harga Sewa"
                      required
                      name={`venue_vendor_items.${index}.price`}
                      register={register}
                      error={errors}
                      statusError={errors?.venue_vendor_items?.[index]?.price}
                    />
                  </Grid>
                  <Grid item xs={12} md={6} className="text-left !mb-4">
                    <div className={`space-y-1 text-xs flex flex-col`}>
                      <label>Foto Detail Barang/Jasa</label>
                      <ImageUpload
                        images={images && images[index]}
                        setImages={(itemImage) => setImages({...images, [index]: itemImage})}
                        onUpload={(item, indexImage, itemList) =>
                          handleImage(item, "upload", indexImage, index, itemList)
                        }
                      />
                    </div>
                  </Grid>
                  {arrList.map((item, indexArr) => {
                    return (
                      <Grid
                        key={indexArr}
                        item
                        xs={12}
                        sm={4}
                        md={3}
                        className="text-left"
                      >
                        <div className="flex items-center justify-center w-full h-[120px] border-2 rounded-lg border-gray-900 overflow-hidden">
                          {!images?.[index]?.[indexArr] ? (
                            <ImagePlaceIcon className="w-12" />
                          ) : (
                            <Image
                              alt="iventori"
                              src={images?.[index]?.[indexArr]?.data_url || "/logoDark.png"}
                              width={244}
                              height={244}
                              objectFit="cover"
                            />
                          )}
                        </div>
                      </Grid>
                    );
                  })}
                  {fields.length > 1 && (
                    <Grid item xs={12} className="text-left !mt-4 ">
                      <Button
                        onClick={() => handleRemove(index)}
                        color="red"
                        text="Hapus Ruangan"
                      />
                    </Grid>
                  )}
                </Grid>
              );
            })}
          </div>
          <Grid item xs={12} className="text-left !mb-4 !mt-4">
            <Button
              onClick={handleSubmit((data) => handleStepRegister(data, "add"))}
              color="input"
              text="Tambah item"
            />
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
              onClick={handleSubmit(handleStepRegister)}
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default TalentPhoto;
