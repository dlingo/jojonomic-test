import Button from "components/Button";
import CardSignup from "./CardSignup";
import { useRouter } from "next/router";

const MainSignup = () => {
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };
  const handleBack = () => {
    router.back()
  }

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 md:w-[525px] xs:px-6 xs:w-[400px] md:px-8">
        <div className="space-y-3">
          <div className="grid grid-cols-5  items-center justify-center">
            <a
              onClick={handleBack}
              className="text-[10px] xs:text-xs text-left text-primary-orange font-medium cursor-pointer"
            >
              Kembali
            </a>
            <h1 className="text-base xs:text-xl sm:text-2xl font-bold col-span-3">
              Daftar Sebagai
            </h1>
          </div>
          <p className="font-semibold text-xs sm:text-sm">Partner</p>
          <h3 className="font-medium text-xs sm:text-sm">
            Pilih jenis Partner
          </h3>
        </div>
        <div className="mt-8 space-y-4 mb-12">
          <div>
            <Button
              onClick={() => handleLink("/signup/talent?step=talent-info")}
              color="primary"
              text="Talent"
            />
          </div>
          <div>
            <Button
              onClick={() => handleLink("/signup/venue?step=venue-info")}
              color="primary"
              text="Venue"
            />
          </div>
          <div>
            <Button
              onClick={() => handleLink("/signup/vendor?step=vendor-info")}
              color="primary"
              text="Vendor"
            />
          </div>
          <div>
            <Button
              onClick={() =>
                handleLink("/signup/influencer?step=influencer-info")
              }
              color="primary"
              text="Influencer"
            />
          </div>
        </div>
      </CardSignup>
    </section>
  );
};

export default MainSignup;
