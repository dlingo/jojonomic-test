import isEmpty from "lodash/isEmpty";

export const signup = (data, type) => {
  let signupData;
  let stepPhoto = [];
  if (type === "talent") {
    const talent = { ...data.talent };
    talent.stepTwo.partner.city_id = Number(talent.stepTwo.partner.city_id);
    talent.stepOne.partner.category_id = Number(
      talent.stepOne.partner.category_id
    );
    talent.stepTwo.talent.price_rate = Number(talent.stepTwo.talent.price_rate);
    talent.stepTwo.talent.religion_id = Number(
      talent.stepTwo.talent.religion_id
    );
    if (talent?.talentSocmed?.partner_socmeds) {
      talent?.talentSocmed?.partner_socmeds.forEach((item) => {
        item.followers = Number(item.followers) || 0;
        item.socmed_id = Number(item.socmed_id) || 0;
      });
    }
    if (talent?.stepPhoto) {
      talent?.stepPhoto?.forEach((image) => {
        stepPhoto.push(image.filename);
      });
    }
    signupData = {
      partner: {
        account_number: "",
        bank_id: 0,
        name: talent.stepOne.user.name,
        rating: 0,
        ...talent.stepOne.partner,
        ...talent.stepTwo.partner,
      },
      partner_socmeds: talent?.talentSocmed?.partner_socmeds || [],
      register_as: "talent",
      talent: {
        talent_images: [...stepPhoto],
        ...talent.stepRider,
        ...talent.stepOne.talent,
        ...talent.stepTwo.talent,
      },
      user: {
        ...talent.stepOne.user,
        ...talent.stepTwo.user,
        email: !isEmpty(talent.talentReplaceEmail?.replaceEmail)
          ? talent.talentReplaceEmail?.replaceEmail
          : talent.stepOne.user.email,
        // email: 'a21@mailinator.com',
        // username: 'faksedassssas2'
      },
    };
  }
  if (type === "influencer") {
    const influencer = { ...data.influencer };
    influencer.stepTwo.partner.city_id = Number(
      influencer.stepTwo.partner.city_id
    );
    influencer.stepOne.partner.category_id = Number(
      influencer.stepOne.partner.category_id
    );
    influencer.stepTwo.influencer.religion_id = Number(
      influencer.stepTwo.influencer.religion_id
    );
    influencer.stepTwo.influencer.marital_id = Number(
      influencer.stepTwo.influencer.marital_id
    );
    if (influencer?.influencerSocmed?.partner_socmeds) {
      influencer?.influencerSocmed?.partner_socmeds.forEach((item) => {
        item.followers = Number(item.followers) || 0;
        item.socmed_id = Number(item.socmed_id) || 0;
      });
    }
    if (influencer?.stepService?.influencer_services) {
      influencer?.stepService?.influencer_services.forEach((item) => {
        item.post_duration = Number(item.post_duration) || 0;
        item.price_rate = Number(item.price_rate) || 0;
        item.total_post = Number(item.total_post) || 0;
      });
    }
    if (influencer?.stepPhoto) {
      influencer?.stepPhoto?.forEach((image) => {
        stepPhoto.push(image.filename);
      });
    }
    signupData = {
      influencer: {
        ...influencer.stepService,
        ...influencer.stepOne.influencer,
        ...influencer.stepTwo.influencer,
        influencer_images: [...stepPhoto],
      },
      partner: {
        account_number: "",
        bank_id: 0,
        name: influencer.stepOne.user.name,
        rating: null,
        ...influencer.stepOne.partner,
        ...influencer.stepTwo.partner,
      },
      partner_socmeds: influencer?.talentSocmed?.partner_socmeds || [],
      register_as: "influencer",
      user: {
        ...influencer.stepOne.user,
        ...influencer.stepTwo.user,
        email: !isEmpty(influencer.influencerReplaceEmail?.replaceEmail)
          ? influencer.influencerReplaceEmail?.replaceEmail
          : influencer.stepOne.user.email,
        // email: 'a121@mailinator.com',
        // username: 'wkwkwks'
      },
    };
  }
  if (type === "venue") {
    const venue = { ...data.venue };
    venue.stepOne.partner.city_id = Number(venue.stepOne.partner.city_id);
    venue.stepOne.partner.category_id = Number(
      venue.stepOne.partner.category_id
    );

    if (venue?.venueSocmed?.partner_socmeds) {
      venue?.venueSocmed?.partner_socmeds.forEach((item) => {
        item.followers = Number(item.followers) || 0;
        item.socmed_id = Number(item.socmed_id) || 0;
      });
    }
    if (venue?.stepFour?.venue_vendor_items) {
      venue?.stepFour?.venue_vendor_items.forEach((item, index) => {
        item.area_size = Number(item.area_size) || 0;
        item.capacity = Number(item.capacity) || 0;
        item.price = Number(item.price) || 0;
        item.quota = Number(item.quota) || 0;
        item.minimum_order = Number(item.minimum_order) || 0;
        if (venue?.stepPhoto?.[index]) {
          const arrImage = [];
          venue?.stepPhoto?.[index]?.forEach((item) =>
            arrImage.push(item.filename)
          );
          item.venue_vendor_item_images = arrImage || [];
        }
      });
    }
    signupData = {
      partner: {
        account_number: null,
        bank_id: null,
        rating: null,
        name: venue.stepOne.user.name,
        ...venue.stepOne.partner,
        ...venue.stepTwo.partner,
        ...venue.stepDetail.partner,
      },
      partner_socmeds: venue?.venueSocmed?.partner_socmeds || [],
      register_as: "venue",
      user: {
        ...venue.stepOne.user,
        ...venue.stepTwo.user,
        ...venue.stepDetail.user,
        email: !isEmpty(venue.venueReplaceEmail?.replaceEmail)
          ? venue.venueReplaceEmail?.replaceEmail
          : venue.stepOne.user.email,
      },
      venue: {
        ...venue.stepDetail.venue,
        ...venue?.stepFour,
      },
    };
  }
  if (type === "vendor") {
    const vendor = { ...data.vendor };
    console.log("test", vendor)
    vendor.stepTwo.partner.city_id = Number(vendor.stepTwo.partner.city_id);
    vendor.stepOne.partner.category_id = Number(
      vendor.stepOne.partner.category_id
    );

    if (vendor?.venueSocmed?.partner_socmeds) {
      vendor?.venueSocmed?.partner_socmeds.forEach((item) => {
        item.followers = Number(item.followers) || 0;
        item.socmed_id = Number(item.socmed_id) || 0;
      });
    }
    if (vendor?.stepFour?.venue_vendor_items) {
      vendor?.stepFour?.venue_vendor_items.forEach((item, index) => {
        item.area_size = Number(item.area_size) || 0;
        item.capacity = Number(item.capacity) || 0;
        item.price = Number(item.price) || 0;
        item.quota = Number(item.quota) || 0;
        item.minimum_order = Number(item.minimum_order) || 0;
        if (vendor?.stepPhoto?.[index]) {
          const arrImage = [];
          vendor?.stepPhoto?.[index]?.forEach((item) =>
            arrImage.push(item.filename)
          );
          item.venue_vendor_item_images = arrImage || [];
        }
      });
    }
    signupData = {
      partner: {
        account_number: null,
        bank_id: null,
        rating: null,
        name: vendor.stepOne.user.name,
        ...vendor.stepOne.partner,
        ...vendor.stepTwo.partner,
        ...vendor.stepDetail.partner,
      },
      partner_socmeds: vendor?.venueSocmed?.partner_socmeds || [],
      register_as: "venue",
      user: {
        ...vendor.stepOne.user,
        ...vendor.stepTwo.user,
        ...vendor.stepDetail.user,
        email: !isEmpty(vendor.venueReplaceEmail?.replaceEmail)
          ? vendor.venueReplaceEmail?.replaceEmail
          : vendor.stepOne.user.email,
        email: "a21121@mailinator.com",
        username: "wkwkswks",
      },
      venue: {
        ...vendor.stepDetail.venue,
        ...vendor?.stepFour,
      },
    };
  }

  return signupData;
};
