import { useEffect } from "react";
import Button from "components/Button";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import { ArrowNarrowLeftIcon } from "@heroicons/react/outline";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { replaceEmail } from "@/redux/signup/action";
import { userRegister } from "@/redux/user/action";
import { signup } from "./utils";
import { fetcher } from "src/utils/fetch";
import ErrorMessage from "@/components/ErrorMessage";
import { CircleSpinIcon } from "@/components/Icons";
import {
  loadingStatus,
  errorStatus,
  successStatus,
  completedStatus,
} from "src/redux/loader/action";

const CONSTANT_TYPE = "register";

const EmailVerificationForm = () => {
  const { signup: partner, loader, user } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const type = router?.query?.type || "/signup";

  const handleLink = (href) => {
    router.push(href);
  };

  const defaultValues = {
    replaceEmail: partner?.[type]?.stepOne?.user?.email || "",
  };
  const handleBack = () => {
    router.back();
  };
  const { register, handleSubmit, setValue } = useForm({
    defaultValues,
  });

  const handleVerification = (data) => {
    dispatch(replaceEmail(data, type));
    const requestBody = signup(partner, type);
    console.log(requestBody)
    dispatch(loadingStatus(CONSTANT_TYPE));

    fetcher("/auth/register-partner", { data: requestBody, method: "POST" })
      .then((res) => {
        dispatch(successStatus(CONSTANT_TYPE));
        dispatch(userRegister(res.data));
        handleLink(`/signup/verification/otp?type=${type}`)
      })
      .catch((err) =>
        dispatch(errorStatus(CONSTANT_TYPE, err?.response?.data?.message))
      );

  };

  useEffect(() => {
    if (type) {
      setValue("replaceEmail", partner?.[type]?.stepOne?.user?.email || "");
    }
  }, [type]);

  useEffect(() => {
    return () => {
      dispatch(completedStatus(CONSTANT_TYPE))
    } 
   },[])

   console.log(user)


  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 md:w-[525px] xs:px-6 xs:w-[400px] md:px-8 min-h-[300px]">
        <div className="">
          <div className="grid grid-cols-5  items-center justify-center mb-8">
            <a
              onClick={handleBack}
              className="text-[10px] xs:text-xs text-left text-primary-orange font-medium cursor-pointer"
            >
              <ArrowNarrowLeftIcon className="w-8" />
            </a>
            <h1 className="text-base xs:text-xl sm:text-2xl font-bold col-span-3">
              Verification
            </h1>
          </div>
          <p className="text-left text-xs mb-8">
            Kami akan mengirimkan kode OTP ke email berikut. Pastikan email
            dibawah sudah benar
          </p>
        </div>
        <form onSubmit={handleSubmit(handleVerification)}>
          {" "}
          <div className="space-y-4 mb-20 text-left">
            <TextField
              placeholder="Masukan Email"
              label="Email"
              labelClass="font-bold text-sm"
              name={`replaceEmail`}
              register={register}
            />
            <ErrorMessage message={loader?.[CONSTANT_TYPE]?.message} />
          </div>
        </form>
        <div className="flex justify-end">
          <div className="w-44">
            <Button
              disabled={loader?.[CONSTANT_TYPE]?.loading}
              loadingIcon={<CircleSpinIcon className="w-5" />}
              type="submit"
              onClick={handleSubmit(handleVerification)}
              color="input"
              text="Kirim"
            />
          </div>
        </div>
      </CardSignup>
    </section>
  );
};

export default EmailVerificationForm;
