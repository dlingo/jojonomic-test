import * as yup from "yup";

export const schemaValidationOne = yup.object().shape({
  user: yup.object({
    name: yup.string().required("harus diisi"),
    username: yup.string().required("harus diisi"),
    email: yup.string().email("Email tidak valid").required("harus diisi"),
    password: yup.string().required("harus diisi"),
    confirm_password: yup
      .string()
      .required("harus diisi")
      .oneOf([yup.ref("password"), null], "konfirmasi sandi tidak sesuai"),
  }),
  talent: yup.object({
    stage_name: yup.string().required("harus diisi"),
    date_of_birth: yup.string().required("harus diisi"),
  }),
  partner: yup.object({
    gender: yup.string().required("harus diisi"),
    category_id: yup.string().required("harus diisi"),
  }),
});
export const schemaValidationTwo = yup.object().shape({
  user: yup.object({
    phone_number: yup
      .string()
      .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
      .max(13, "maksimal 13 digit")
      .min(9, "minimal 9 digit")
      .required("harus diisi"),
    address: yup.string().required("harus diisi"),
  }),
  talent: yup.object({
    religion_id: yup.string().required("harus diisi"),
    price_rate: yup
      .string()
      .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
      .required("harus diisi"),
  }),
  partner: yup.object({
    city_id: yup.string().required("harus diisi"),
    description: yup.string().required("harus diisi"),
  }),
});
