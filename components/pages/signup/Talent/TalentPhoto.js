// import Image from "@/components/Image";
import Image from "next/image";
import { useState, useEffect } from "react";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import { ImagePlaceIcon } from "@/components/Icons";
import ImageUpload from "../ImageUpload";
import { useSelector, useDispatch } from "react-redux";
import { stepPhotoTalent, removePhotoTalent } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import { TrashIcon } from "@heroicons/react/outline";

const arrList = Array(10).fill(null);

const TalentPhoto = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [images, setImages] = useState(partner?.talent?.stepPhoto || []);
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const handleNextStep = () => {
    if (images.length > 0) {
      handleLink("/signup/talent?step=talent-rider");
    }
  };

  const handleImage = (item, type, index) => {
    const method = type === "upload" ? "POST" : "DELETE";
    const formData = new FormData();
    console.log(index)
    if (type === "upload") {
      formData.append("image", item.file, item.file.name);
      fetcher("/auth/upload-image", { method, data: formData })
        .then((res) => {
          dispatch(stepPhotoTalent([res.data], index));
        })
        .catch((err) => {
          console.log("error");
        });
    } else {
      dispatch(removePhotoTalent(index))
      // fetcher("/auth/delete-image", { method, data: item?.filename })
      // .then((res) => {
      //   dispatch(removePhotoTalent(index))
      // })
      // .catch((err) => {
      //   console.log("error");
      // });
    }
  };

  // useEffect(() => {
  //   setImages()
  // }, [partner?.talent?.stepPhoto])
  console.log(images)

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Talent" activeStep={3} />
        <form className="mt-8 space-y-4 mb-12 w-full">
          <div className="flex justify-between items-center">
            <p className="text-xs">Foto (minimal 3)*</p>
            <div className="w-44 relative">
              <ImageUpload
                images={images}
                setImages={setImages}
                onUpload={(item, index) => handleImage(item, "upload", index)}
              />
            </div>
          </div>
          <Grid container spacing={3}>
            {arrList.map((itemImage, index) => {
              return (
                <Grid
                  key={index}
                  item
                  xs={12}
                  sm={4}
                  md={3}
                  className="text-left"
                >
                  <div className="flex items-center justify-center w-full h-[120px] border-2 rounded-lg border-gray-900 overflow-hidden relative">
                    {!images?.[index] ? (
                      <ImagePlaceIcon className="w-12" />
                    ) : (
                      <Image
                        alt="iventori"
                        src={
                          images?.[index]?.url ||
                          images?.[index]?.data_url ||
                          "/logoDark.png"
                        }
                        width={244}
                        height={244}
                        objectFit="cover"
                      />
                    )}
                    {(images?.[index]?.url || images?.[index]?.data_url) && (
                      <ImageUpload
                        images={images}
                        setImages={setImages}
                        iconRemove={
                          <TrashIcon className="absolute top-1 right-1 text-red-500 w-4" />
                        }
                        onRemove={() => handleImage(images?.[index], "remove", index)}
                      />
                    )}
                  </div>
                </Grid>
              );
            })}
          </Grid>
          <div className="flex justify-end">
            <button
              type="button"
              className="text-primary-orange font-semibold text-right active:opacity-70"
              onClick={handleNextStep}
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default TalentPhoto;
