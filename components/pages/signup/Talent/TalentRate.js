import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import ListBox from "pages/signup/ListBox";
import { PlusIcon } from "@heroicons/react/solid";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationTwo } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepTwoTalent, stepSocialMedia } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const SocialMediaDialog = dynamic(() => import("../SocialMediaDialog"));

const TalentRate = () => {
  const { signup: partner } = useSelector((state) => state);
  const { data: cities } = useSWR("/reference/cities", fetcher);
  const { data: religion } = useSWR("/reference/religion", fetcher);
  const [openDialog, setOpenDialog] = useState(false);
  const router = useRouter();
  const dispatch = useDispatch();

  const handleLink = (href) => {
    router.push(href);
  };

  // const socMedList = partner?.talent?.talentSocmed || null
  const defaultValues = {
    user: {
      phone_number: partner?.talent?.stepTwo?.user?.phone_number || "",
      address: partner?.talent?.stepTwo?.user?.address || "",
    },
    talent: {
      religion_id: partner?.talent?.stepTwo?.talent?.religion_id || "",
      price_rate: partner?.talent?.stepTwo?.talent?.price_rate || "",
    },
    partner: {
      city_id: partner?.talent?.stepTwo?.partner?.city_id || "",
      description: partner?.talent?.stepTwo?.partner?.description || "",
    },
    list: { ...partner?.talent?.stepTwo?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationTwo),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepTwoTalent(data));
    handleLink("/signup/talent?step=talent-photo");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Talent" activeStep={2} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nomor Telepon"
                label="Nomor Telepon"
                name="user.phone_number"
                register={register}
                prefixPhone
                required
                error={errors}
                statusError={errors?.user?.phone_number}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Agama"
                placeholder="Pilih Agama"
                listOption={religion?.data || []}
                name="talent.religion_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.talent?.religion_id}
                defaultValues={defaultValues?.list?.talent?.religion_id}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Domisili"
                placeholder="Pilih Domisili"
                listOption={cities?.data || []}
                name="partner.city_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.city_id}
                defaultValues={defaultValues?.list?.partner?.city_id}
              />
            </Grid>
            <Grid item xs={12} md={8} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Alamat Sesuai KTP"
                label="Alamat Sesuai KTP"
                name="user.address"
                register={register}
                required
                error={errors}
                statusError={errors?.user?.address}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Price Rate"
                label="Price Rate"
                name="talent.price_rate"
                register={register}
                required
                error={errors}
                statusError={errors?.talent?.price_rate}
              />
            </Grid>
            <Grid item xs={12} md={8} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Deskripsi"
                label="Deskripsi"
                name="partner.description"
                register={register}
                required
                error={errors}
                statusError={errors?.partner?.description}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <div className={`space-y-1 text-xs flex flex-col`}>
                <label>Akun Sosial Media</label>
                <button
                  onClick={() => setOpenDialog(!openDialog)}
                  type="button"
                  className="justify-center border-1 p-1 rounded-lg border-primary-orange w-full bg-primary-orange focus:outline-none focus:border-primary-orange flex items-center active:opacity-70"
                >
                  <PlusIcon className="text-white w-6" />
                </button>
              </div>
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
      <SocialMediaDialog
        open={openDialog}
        setOpen={setOpenDialog}
        dispatchSocMed={(data) => dispatch(stepSocialMedia(data, 'talent'))}
        listValue={null}
      />
    </section>
  );
};

export default TalentRate;
