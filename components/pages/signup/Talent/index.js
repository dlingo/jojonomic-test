import { useEffect } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

const TalentInfo = dynamic(import('./TalentInfo'))
const TalentRate = dynamic(import("./TalentRate"));
const TalentPhoto = dynamic(import("./TalentPhoto"));
const TalentRider = dynamic(import("./TalentRider"));

export default function Talent() {
  const router = useRouter()
  const step = router.query?.step

  if (step === "talent-info") {
    return <TalentInfo />;
  }
  if (step === "talent-rate") {
    return <TalentRate />;
  }
  if (step === "talent-photo") {
    return <TalentPhoto />;
  }
  if (step === "talent-rider") {
    return <TalentRider />;
  }

  return <div>loading...</div>
}
