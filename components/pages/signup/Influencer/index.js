import { useEffect } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

const InfluencerInfo = dynamic(import("./InfluencerInfo"));
const InfluencerRate = dynamic(import("./InfluencerRate"));
const InfluencerPhoto = dynamic(import("./InfluencerPhoto"));
const InfluencerService = dynamic(import("./InfluencerService"));

export default function Influencer() {
  const router = useRouter();
  const step = router.query?.step;

  if (step === "influencer-info") {
    return <InfluencerInfo />;
  }
  if (step === "influencer-rate") {
    return <InfluencerRate />;
  }
  if (step === "influencer-photo") {
    return <InfluencerPhoto />;
  }
  if (step === "influencer-service") {
    return <InfluencerService />;
  }

  return <div>loading...</div>;
}
