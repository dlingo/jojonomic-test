import Button from 'components/Button'
import CardSignup from './CardSignup';
import { useRouter } from 'next/router'

const MainSignup = () => {
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  }

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 md:w-[525px] xs:px-6 xs:w-[400px] md:px-8">
        <div className="text-center space-y-2">
          <h1 className="text-xl xs:text-2xl sm:text-3xl font-bold">
            Daftar Sekarang
          </h1>
          <p className="text-xs sm:text-base">
            <span>Sudah Punya Akun? </span>
            <span>
              <a className="text-primary-orange font-bold">Masuk</a>
            </span>
          </p>
        </div>
        <div className="mt-8 space-y-4 mb-12">
          <h3 className="font-medium text-base sm:text-lg">Daftar Sebagai</h3>
          <div>
            <Button
              onClick={() => handleLink("/signup/partner")}
              color="primary"
              text="Partner"
            />
          </div>
          <p className="text-gray-400 text-xs sm:text-base">Atau</p>
          <div>
            <Button
              onClick={() => handleLink("/signup/user")}
              color="primary"
              text="User"
            />
          </div>
        </div>
      </CardSignup>
    </section>
  );
}

export default MainSignup