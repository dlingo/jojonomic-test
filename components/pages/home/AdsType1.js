import React from "react";
import Image from "next/image";

export default function AdsType1({ src, alt }) {
  return (
    <section className="w-screen flex justify-center">
      <Image width={1146} height={300} objectFit="cover" src={src} alt={alt} />
    </section>
  );
}
