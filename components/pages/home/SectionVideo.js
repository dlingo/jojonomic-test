import React from "react";

export default function SectionVideo() {
  return (
    <>
    <section className="container-content ">
        <div className="text-3xl sm:text-4xl lg:text-[2.7rem] xl:text-[3.5rem]">
          <div className="">
            <div className="sm:grid sm:grid-cols-2 h-auto sm:h-[8.4em] mt-[1em] mb-[7em] sm:mb-[1em] leading-[1.1] relative">
              <div className="sm:w-[calc(148%)] p-[1.43em] pt-[1em] sm:pt-[1.61em] bg-gradient-to-r from-primary-orange via-primary-orange to-orange-400 rounded-[0.8em]">
                <div className="sm:w-[calc(70%)] xl:w-[600px]">
                  <h1 className="font-pop text-white text-center sm:text-left font-bold">
                    Eventori.id Indonesia Entertainment Ecosystem
                  </h1>
                </div>
              </div>
              <div className="flex justify-center sm:items-center">
                <iframe
                  className="w-[calc(90%)] sm:w-full xl:max-w-[600px] h-[7em] sm:h-3/4 bg-primary-green rounded-[0.8em] top-[calc(85%)] absolute sm:static"
                  src="https://www.youtube.com/embed/esuhiAwx_fw?mute=1&rel=0"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
