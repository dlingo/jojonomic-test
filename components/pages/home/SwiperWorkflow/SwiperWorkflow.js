// add home component/ swiper workflow
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination } from "swiper";

import "swiper/css/bundle";
import styles from "./SwiperWorkflow.module.css";
import CardWorkflow from "../Card/CardWorkflow";

import DataBenefit from "data/DummyContentHome/DataBenefit.json"

SwiperCore.use([Navigation, Pagination]);
export default function SwiperWorkflow() {
  console.log(DataBenefit.data);

  const slides = [];

  // for (let i = 0; i < 5; i++) {
  //   slides.push(
  //     <SwiperSlide key={`slide-${i}`} className="pb-5" >
  //       <CardWorkflow />
  //     </SwiperSlide>
  //   );
  // }

  DataBenefit.data.map((item, index)=>{
    slides.push(
      <SwiperSlide key={`slide-${index}`} className="pb-5" >
        <CardWorkflow data={item}/>
      </SwiperSlide>
    );
  })

  return (
    <div className="w-[90vw] md:w-[80vw] max-w-[1152px] overflow-hidden">
      {/* <div className="md:w-[130vw] relative md:right-[27vw] max-w-[1872px] xxl:right-[370px]"> */}
      <div className="md:w-[131vw] relative md:right-[25vw] max-w-[1872px] xxl:right-[370px]">
        <Swiper
          id="main"
          modules={[Navigation, Pagination]}
          navigation
          slidesPerView={3}
          loop
          //   centeredSlides
          className={styles.swiperHome}
          breakpoints={{
            0: {
              slidesPerView: 1,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
          }}
        >
          {slides}
        </Swiper>
      </div>
    </div>
  );
}
