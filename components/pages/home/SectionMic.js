import React from "react";
import Image from "next/image";

export default function SectionMic() {
  return (
    <>
      <section className="w-screen flex justify-center">
        <div className="relative h-[467px] xl:h-[658px]">
          <Image
            width={1440}
            height={658}
            objectFit="cover"
            src={"/micBg.png"}
            className="absolute"
          />
          {/* <div className="absolute top-0 text-white">
            alas
          </div> */}
          <div
            className="w-[520px] xl:w-[742px] h-[467px] xl:h-[658px] mt-[80px] xl:mt-[240px] mr-[38px] text-center absolute top-0 right-0"
            style={{
              fontFamily: "Poppins",
              // display: "none",
            }}
          >
            <h1 className="text-5xl text-white font-bold leading-tight">
              Kami ada untuk kamu yang sedang mewujudkan impian.
            </h1>
            {/* <h1 className="text-5xl text-white font-bold leading-snug"></h1> */}
            <button
              className="w-80 h-12 bg-primary-orange rounded-lg text-xl font-bold text-white mt-[45px] xl:mt-[90px]"
              style={{ fontFamily: "Open Sans" }}
            >
              Daftar Sekarang
            </button>
          </div>
        </div>
      </section>
    </>
  );
}
