import CustomCardHeader from "components/CustomCard/CustomCardHeader";
import CustomCard from "/components/CustomCard";
import Link from "next/link";

const SectionPartner = ({ dataCategory, TipePartner, title }) => {
  // const category = dataCategory.partnerType.toLowerCase();
  console.log(dataCategory);
  const handleStyle = () => {
    if (TipePartner == "talent" || TipePartner == "influencer") {
      return {
        cardGrid: "grid-cols-2 md:grid-cols-3 gap-4",
      };
    }

    if (TipePartner == "venue" || TipePartner == "vendor") {
      return {
        cardGrid: "grid-cols-2 md:grid-cols-3 lg:grid-cols-4",
      };
    }
  };

  return (
    <section className="container-content">
      <CustomCardHeader
        title={title}
        readMore={TipePartner}
        type={TipePartner}
      />
      <div className={`grid ${handleStyle().cardGrid} gap-4 xl:gap-6`}>
        {dataCategory.data.map((item, key) => {
          return (
            <Link
              key={key}
              href={`/partner/${TipePartner}/${item.id}/${item.name}`}
            >
              <a>
                <CustomCard type="partner" category={TipePartner} data={item} />
              </a>
            </Link>
          );
        })}
      </div>
    </section>
  );
};

export default SectionPartner;
