import React from "react";
import {
  TextField,
  Grid,
  Button,
  Select,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import { ArrowForward, Search } from "@material-ui/icons";

import DataFilterSearch from "../../../../data/DataFilterSearch.json";

export default function TabVenueContent() {
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <form noValidate autoComplete="off" style={{ width: "100%" }}>
      <Grid container spacing={2} direction="row">
        <Grid item lg={6}>
          <div style={{ fontFamily: "Open Sans" }}>Nama Venue</div>
          <TextField
            fullWidth
            id="outlined-basic"
            variant="outlined"
            size="small"
            placeholder="Masukkan Nama Venue Disini"
          />
        </Grid>
        <Grid item lg={6}>
          <FormControl fullWidth>
            <div style={{ fontFamily: "Open Sans" }}>Tanggal Acara</div>
            <TextField
              id="date"
              size="small"
              type="date"
              variant="outlined"
              defaultValue=""
              InputLabelProps={{
                shrink: true,
              }}
            />
          </FormControl>
        </Grid>
        <Grid item lg={6}>
          <FormControl fullWidth size="small">
            <div style={{ fontFamily: "Open Sans" }}>Kategori</div>
            <Select
              onChange={handleChange}
              displayEmpty
              variant="outlined"
              inputProps={{ "aria-label": "Without label" }}
            >
              <MenuItem disabled>
                Pilih Kategori Venue (contoh: Hotel, Mall)
              </MenuItem>
              {DataFilterSearch.venue.map((data, index) => {
                return (
                  <MenuItem key={index} value={data.id}>
                    {data.item}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={6}>
          <FormControl fullWidth size="small">
            <div style={{ fontFamily: "Open Sans" }}>Lokasi</div>
            <Select
              onChange={handleChange}
              displayEmpty
              variant="outlined"
              inputProps={{ "aria-label": "Without label" }}
            >
              <MenuItem disabled>Pilih Lokasi Venue</MenuItem>
              {DataFilterSearch.lokasi.map((data, index) => {
                return (
                  <MenuItem key={index} value={data.id}>
                    {data.item}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <Grid
        container
        justify="flex-end"
        direction="row"
        style={{ marginTop: 20 }}
      >
        <Grid item lg={3}>
          <Button fullWidth variant="contained" color="primary">
            <Grid container justifycontent="flex-start">
              <Grid item>
                <Search />
                Cari Venue
              </Grid>
            </Grid>
            <ArrowForward />
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
