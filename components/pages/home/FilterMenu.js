import { Tab } from "@headlessui/react";
import TabPanel from "@/components/pages/home/TabsContent/TabPanel";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const tabList = [
  {
    id: 1,
    name: "Talent",
    icon: "",
  },
  {
    id: 2,
    name: "Venue",
    icon: "",
  },
  {
    id: 3,
    name: "Vendor",
    icon: "",
  },
  {
    id: 4,
    name: "Influencer",
    icon: "",
  },
];

export default function FilterMenu({ data }) {
  return (
    <div className="container-fluid w-full pt-6 md:absolute md:bottom-[-8.75rem] z-10">
      <div className=" max-w-7xl m-auto bg-transparent rounded-2xl overflow-hidden shadow-lg">
        <Tab.Group>
          <Tab.List className="flex bg-primary-orange md:max-w-[50%] rounded-tr-2xl">
            {tabList.map((item, index) => {
              return (
                <Tab
                  key={index}
                  className={({ selected }) =>
                    classNames(
                      "text-xs sm:text-sm",
                      "w-full py-3 leading-5 font-medium text-primary-orange",
                      "focus:outline-none",
                      selected
                        ? "bg-white last:rounded-tr-2xl"
                        : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                    )
                  }
                >
                  {item.name}
                </Tab>
              );
            })}
          </Tab.List>
          <Tab.Panels className="p-6 px-8 bg-white shadow-lg md:rounded-tr-2xl">
            <Tab.Panel>
              <TabPanel label="Nama Talent" textBtn="Cari Talent" data={data} />
            </Tab.Panel>
            <Tab.Panel>
              <TabPanel label="Nama Venue" textBtn="Cari Venue" data={data} />
            </Tab.Panel>
            <Tab.Panel>
              <TabPanel label="Nama Vendor" textBtn="Cari Vendor" data={data} />
            </Tab.Panel>
            <Tab.Panel>
              <TabPanel
                label="Nama Influencer"
                textBtn="Cari Influencer"
                data={data}
              />
            </Tab.Panel>
          </Tab.Panels>
        </Tab.Group>
      </div>
    </div>
  );
}
