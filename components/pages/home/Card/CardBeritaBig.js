import React from "react";
import Image from "next/image";

export default function CardBeritaBig({ src, alt, title }) {
  return (
    <div className="flex flex-col items-center">
      {/* <div className="w-[342px] h-[280px] md:w-[550px] lg:w-[545px] md:h-[450px] xl:w-[758px] xl:h-[622px] bg-primary-orange rounded-2xl"> */}
      <div className="w-full h-full bg-primary-orange rounded-2xl relative cursor-pointer">
        <Image
          src={src}
          width={755}
          height={490}
          // alt={`${data.name} Product Picture`}
          alt={alt}
          layout="responsive"
          objectFit="cover"
          className="rounded-2xl"
        />
        <div className="text-xs md:text-[20px] xl:text-[30px]">
          {/* <div className="h-[58px] md:h-[98px] xl:h-[123px] -mt-1 flex items-center px-[20px] xl:px-[30px]"> */}
          <div className="h-[4em] flex items-center px-[1.2em] md:px-[1em]">
            <h4 className="line-clamp-2 leading-tight font-semibold text-white font-pop">
              {/* Talentori Aqsa Mahesa, Musisi Cilik Asal Nganjuk yang Ingin
              Menjadi Seperti Rhoma Irama */}
              {title}
            </h4>
          </div>
        </div>
      </div>
    </div>
  );
}
