import React from "react";
import Image from "next/image";
import icon from "../../../public/iconWorkflow.png";

export default function StepperHomeContent({contentStepper}) {
  return (
    <>
      <div className="w-[420px]  grid grid-cols-1 relative">
        <div className="rounded-lg h-[180px] sm:h-[210px] box-border border-3 border-primary-orange px-12 pb-7 pt-14 -mt-10">
          <div className="text-center sm:px-10">
            <h2
              className="text-sm sm:text-lg text-primary-orange uppercase font-bold mb-4"
              style={{ fontFamily: "Poppins" }}
            >
              {contentStepper.title}
            </h2>
            <p 
            className="text-xs sm:text-sm"
            style={{ fontFamily: "Open Sans" }}>
              {contentStepper.content}
            </p>
          </div>
        </div>
        <div className="w-full flex justify-center">
          <div className="h-24 w-24 rounded-full border-3 border-inset border-primary-orange bg-primary-white absolute -top-22">
            <div className="h-full w-full flex justify-center items-center">
              <Image src={icon} height={70} width={70} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
