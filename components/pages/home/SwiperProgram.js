import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination } from "swiper";
// import CustomCard from "./Card/CustomCard";
import CustomCard from "components/CustomCard";

import "swiper/css/bundle";

SwiperCore.use([Pagination]);

export default function SwiperProgram({ data }) {
  const list = data;

  return (
    <>
      <Swiper
        modules={[Pagination]}
        pagination={{ clickable: true }}
        slidesPerView={3}
        slidesPerGroup={3}
        spaceBetween={16}
        breakpoints={{
          0: {
            slidesPerView: 2,
            slidesPerGroup: 2,
          },
          768: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            spaceBetween: 16,
          },
          1440: {
            spaceBetween: 35,
          },
        }}
        loop
      >
        {list.data.map((list, key) => {
          return (
            <SwiperSlide key={key} style={{ paddingBottom: 60 }}>
              {/* <CustomCard cardProgram data={item} /> */}
              <CustomCard
                type="program"
                data={list.title}
                src={list.thumbnail}
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </>
  );
}
