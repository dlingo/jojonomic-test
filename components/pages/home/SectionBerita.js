import React from "react";
import CardBeritaBig from "./Card/CardBeritaBig";
import CardBeritaSmall from "./Card/CardBeritaSmall";
// import CustomCardHeader from "./Card/CustomCardHeader";
import CustomCardHeader from "components/CustomCard/CustomCardHeader";
// import Data from "../../../data/DummyContentHome/DataBerita.json"

export default function SectionBerita({ data }) {
  // list.reverse();
  // const list2 = list.slice(0, -1);
  // list2.reverse();

  return (
    <>
      <section
        // className="w-screen px-4 md:px-28 lg:px-24 xl:px-36 py-6 sm:py-12 "
        className="container-content"
      >
        <CustomCardHeader title="Berita Terbaru" readMore="berita" />
        <div className="grid grid-rows-3 lg:grid-rows-2 lg:grid-cols-3 lg:grid-flow-col gap-4 md:gap-6">
          <div className="row-span-2 col-span-2  flex justify-center lg:justify-end">
            {/* {list.map((item, i, arr) => {
              console.log(list);
              if (arr.length === i + 1) {
                return ( */}
            <CardBeritaBig
              key={data?.data?.[0]?.id}
              src={data?.data?.[0]?.images_thumbnail}
              alt={data?.data?.[0]?.images_alt}
              title={data?.data?.[0]?.title}
            />
            {/* );
              } else {
                null;
              }
            })} */}
          </div>
          {data.data.map((item, index) => {
            if (index > 0) {
              return (
                <div key={index} className="flex justify-end lg:justify-start">
                  <CardBeritaSmall
                    src={item.images_thumbnail}
                    alt={item.images_alt}
                    title={item.title}
                  />
                </div>
              );
            }
          })}
        </div>
      </section>
    </>
  );
}
