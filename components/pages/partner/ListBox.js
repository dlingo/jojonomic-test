import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { DropdownIcon } from "@/components/Icons";

export default function ListBox({ defaultValues, placeholder, listOption }) {
  const [selected, setSelected] = useState(
    defaultValues ? { name: defaultValues } : null
  );
  // console.log(selected);

  return (
    <div className="text-xs space-y-1">
      <Listbox value={selected} onChange={setSelected}>
        <div className="relative">
          <Listbox.Button
            className={`border-1 py-2 px-4 md:px-6 rounded-lg border-gray-900 w-full h-full focus:outline-none focus:border-primary-orange text-left`}
          >
            <span
              className={`block truncate text-xs md:text-sm xl:text-lg text-black`}
            >
              {selected?.name || placeholder}
            </span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-4 md:pr-6 pointer-events-none">
              <DropdownIcon
                className="w-2 sm:w-3 lg:w-4 text-black"
                aria-hidden="true"
              />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute w-full py-2 mt-2 border-1 border-gray-300 overflow-auto bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none z-50">
              {listOption?.map((option, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({ active }) =>
                    `${
                      active
                        ? "text-amber-900 bg-amber-100  bg-opacity-60"
                        : "text-gray-900 "
                    }
                          cursor-pointer select-none relative px-2`
                  }
                  value={option}
                >
                  {({ selected, active }) => (
                    <>
                      <span
                        className={`p-2 text-base ${
                          selected
                            ? "font-medium bg-orange-10 rounded-md text-black"
                            : "font-normal"
                        } block truncate`}
                      >
                        {option.name}
                      </span>
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
}
