import React from "react";
import Banner from "@/components/Banner/";
import Picture from "../ProfilePicture";
import Summary from "../detail/PartnerSummary";
import Service from "../detail/InfluencerDetail";
import Ulasan from "../ulasan";
import Data from "data/DummyContentPartner/DataPartner.json";
import { useRouter } from "next/router";

export default function MainDetailInfluencer({ data }) {
  const dataInfluencer = Data.items[3];
  const path = useRouter();

  return (
    <>
      <Banner
        imgSrc={dataInfluencer.imgBanner}
        slug={path.asPath}
        title={data?.data?.partner?.name}
        partner={dataInfluencer.partnerType}
        type={data?.data?.partner?.partner_category?.type_categories}
      />
      {/* atas */}
      <section className="container-fluid pt-10 md:pt-[90px] xl:pt-[135px] pb-8 md:pb-[60px] xl:pb-[92px] grid md:flex">
        <Picture data={dataInfluencer} ButtonText="Lihat Galeri" />
        <Summary data={data} type={dataInfluencer.partnerType} />
      </section>
      
      {/* bawah */}
      <Service data={data} type={dataInfluencer.partnerType} />
      <Ulasan data={dataInfluencer} />
    </>
  );
}
