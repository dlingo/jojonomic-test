import CustomCard from "@/components/CustomCard";
import Button from "components/Button";
import { PlayCircleIcon } from "@/components/Icons";
import Link from "next/link";

const TalentPicture = ({ data, ButtonText = "Lihat" }) => {
  return (
    <div className="w-full md:w-[calc(41%)] xl:max-w-[471px] flex-shrink-0 mr-8 relative">
      {/* <div className="w-[471px] flex-shrink-0 mr-8 relative"> */}
      <CustomCard type="detail" shade="20" data={data} />
      <div className="w-full md:absolute bottom-0 mt-6">
        <div className="flex justify-between">
          <div className="w-[calc(56%)]">
            <Link href="/partner/talent/1/gallery/test">
              <a>
                <Button color="primary" text={ButtonText} />
              </a>
            </Link>
          </div>
          <div className="w-[calc(44%-9px)]">
            <Button
              color="primary"
              text="Feed"
              iconPrefix={
                <PlayCircleIcon className="w-4 md:w-6 text-primary-orange group-hover:text-white" />
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TalentPicture;
