import React from 'react'
import ListPartner from "../ListPartner";
import Data from "data/DummyContentPartner/DataTalentAll.json";
// import Data from "data/DummyContentPartner/DataVenueAll.json";

export default function ListVenue() {
    return (
        <ListPartner data={Data} />
    )
}

