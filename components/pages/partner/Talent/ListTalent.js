import React from 'react'
import ListPartner from "../ListPartner";
import Data from "data/DummyContentPartner/DataTalentAll.json";
// import Data from "data/DummyContentPartner/DataInfluencerAll.json";

export default function ListTalent() {
    return (
        <ListPartner data={Data} />
    )
}

