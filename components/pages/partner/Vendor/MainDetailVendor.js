import React from "react";
import Banner from "@/components/Banner/";
import Picture from "../ProfilePicture";
// import Summary from "./Detail/VendorSummary";
// import Detail from "./Detail/VendorDetailDesc";
import Summary from "../detail/PartnerSummary";
import Detail from "../detail/PartnerDetailDesc";
import Ulasan from "../ulasan";
// import Data from "data/DummyContentPartner/DataTalentAll.json";
import Data from "data/DummyContentPartner/DataPartner.json";

export default function MainDetailVendor() {
  const dataVendor = Data.items[2];
  return (
    <>
      <Banner imgSrc={dataVendor.imgBanner} slug title={dataVendor.type} partner={dataVendor.partnerType} type={dataVendor.speciality} />
      {/* atas */}
      <section className="container-fluid pt-10 md:pt-[90px] xl:pt-[135px] pb-8 md:pb-[60px] xl:pb-[92px] grid md:flex">
        <Picture data={dataVendor} ButtonText="Lihat Seluruh Ruangan" />
        <Summary data={dataVendor} type={dataVendor.partnerType}/>
      </section>
      {/* bawah */}
      <Detail data={dataVendor} type={dataVendor.partnerType}/>
      <Ulasan data={dataVendor} />
    </>
  );
}
