import React from "react";
import Detail from "./Detail";
import Button from "components/Button"

export default function index() {
  return (
    <>
      <div className="pb-8 lg:pb-14 xl:pb-16">
        <h1 className="text-18-responsive font-bold lg:pb-2 xl:pb-4">Seluruh Ulasan</h1>
        <Detail />
        <Detail />
        <Detail />
        <Detail />
        <Detail half />
      </div>
      <div className="md:w-[calc(45%)] xxl:w-[421px] ">
        <Button color="primary" text="Lihat Seluruh Ulasan" />
      </div>
    </>
  );
}
