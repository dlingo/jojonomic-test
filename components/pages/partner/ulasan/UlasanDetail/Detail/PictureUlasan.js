import Image from "next/image";

const PictureUlasan = ({ quantity, className }) => {
  const dummyImg =
    "https://images.unsplash.com/photo-1598342473022-5ced5bb5c291?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80";
  return (
    <div className={`${className} relative` }>
      <Image
        src={dummyImg}
        width={100}
        height={100}
        alt={`Content Picture`}
        layout="responsive"
        objectFit="cover"
        className="absolute inset-0 rounded-2xl cursor-pointer"
      />
      {quantity && (
        <>
          <div
            className={`absolute inset-0 bg-black opacity-50 rounded-2xl cursor-pointer`}
          ></div>
          <div className="absolute inset-0 text-lg font-semibold text-white opacity-50 flex items-center justify-center">
            {quantity}
          </div>
        </>
      )}
    </div>
  );
};

export default PictureUlasan;
