import React from "react";
import Content from "./Content";
import Profil from "./Profil";

export default function index({ half }) {
  return (
    <div
      className={`w-full xxl:w-[1000px] ${
        !half && "border-b-3 border-primary-gray"
      } md:flex items-start mt-4`}
    >
      {/* kiri */}
      <Profil half={half}/>
      {/* kanan */}
      <Content half={half} />
    </div>
  );
}
