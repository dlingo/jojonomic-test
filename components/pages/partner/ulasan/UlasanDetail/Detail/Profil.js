import { AvatarIcon } from "@/components/Icons";
import PictureUlasan from "./PictureUlasan";

const Profil = ({ half }) => {
  return (
    <div className="flex items-center justify-between">
      <div className="flex items-center mb-4">
        <AvatarIcon className="w-12 xl:w-[60px] mr-4" />
        <div>
          <h1 className="w-[108px] text-18-responsive leading-none font-bold line-clamp-1">
            DavidBeats
          </h1>
          <h2 className="text-14-responsive text-primary-gray">2 Hari Lalu</h2>
        </div>
      </div>
      {!half && (
        <PictureUlasan className="w-[80px] relative md:hidden" quantity="2" />
      )}
    </div>
  );
};

export default Profil;
