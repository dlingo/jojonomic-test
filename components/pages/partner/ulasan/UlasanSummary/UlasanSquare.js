import { StarFilled } from "@/components/Icons";

const UlasanSquare = () => {
  return (
    // <div className="px-8 py-10 xl:px-[46px] xl:py-[40px]  border-3 border-orange-500 rounded-lg flex flex-col justify-center items-center text-center">
    <div className="w-[210px] h-[180px] xl:w-[255px] xl:h-[220px] pb-3 xl:pb-4 border-3 border-orange-500 rounded-lg flex flex-col justify-center items-center">
      <StarFilled className="w-10 xl:w-12 text-orange-500" />
      <h1 className="text-48-responsive font-bold leading-snug">4.4 / 5</h1>
      {/* <h1 className="text-5xl font-bold leading-snug">4.4 / 5</h1> */}
      <p className="text-14-responsive font-bold">Ulasan dari 87 Pesanan</p>
    </div>
  );
};

export default UlasanSquare;
