import React from "react";
// import UlasanBar from "./UlasanBar";
import UlasanSquare from "./UlasanSquare";
import UlasanBar from "./UlasanBar";

export default function index() {
  return (
    <div className="pb-6 lg:pb-10 xl:pb-12">
      <h1 className="text-18-responsive font-bold pb-3">Ulasan</h1>
      <div className="grid grid-cols-1 md:flex">
        <div className="flex justify-start md:static">
          <UlasanSquare />
        </div>
        <div className="flex justify-start md:static mt-3 md:mt-0">
          <div className="grid grid-rows-5 items-center">
            <UlasanBar />
            <UlasanBar />
            <UlasanBar />
            <UlasanBar />
            <UlasanBar />
          </div>
        </div>
      </div>
    </div>
  );
}
