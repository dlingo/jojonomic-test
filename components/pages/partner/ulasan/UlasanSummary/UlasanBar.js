import { StarFilled } from "@/components/Icons";

const UlasanBar = () => {
  return (
    <div className="md:ml-8">
      <div className="flex items-center">
        <StarFilled className="w-5 xl:w-6 mb-2 text-orange-500 mr-1 xl:mr-2" />
        <div className="mr-2 md:mr-5 text-24-responsive">5</div>
        <div className="flex items-center mr-2 md:mr-5">
          <div className="relative w-[200px] md:w-[330px] xl:w-[520px] h-3 xl:h-4 ">
            <div className="absolute inset-0 w-full h-full rounded-[7.5px] bg-primary-gray"></div>
            <div className="absolute inset-0 w-3/4 h-full rounded-[7.5px] bg-primary-orange"></div>
          </div>
        </div>
        <div className="text-24-responsive font-bold">51</div>
      </div>
    </div>
  );
};

export default UlasanBar;
