import CustomCardHeader from "components/CustomCard/CustomCardHeader";
import CustomCard from "/components/CustomCard";

const SectionCategory = ({ dataCategory }) => {
  return (
    <div className="my-[20px] md:my-[30px] lg:my-[50px]">
      <CustomCardHeader
        title={dataCategory.category}
        readMore={dataCategory.category}
      />
      <div className="grid grid-cols-2 md:grid-cols-3 gap-4 xl:gap-6">
        {dataCategory.member.map((item, key) => {
          return (
            <CustomCard
              type="partner"
              category="talent"
              data={item}
              key={key}
            />
          );
        })}
      </div>
    </div>
  );
};

export default SectionCategory;
