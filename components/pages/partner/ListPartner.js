import Banner from "@/components/Banner";
import ListBox from "./ListBox";
import SectionCategory from "./PartnerCategory";

export default function MainTalent({data}) {
  const categories = data.items.map((item) => {
    return { name: item.category };
  });

  return (
    <>
      <Banner imgSrc={data.imgBanner} title={data.partnerType} />
      <section className="container-content pb-[60px] md:pb-[80px] lg:pb-[132px]">
        <div className="mt-[calc(1.4%)] mb-[calc(2.3%)]  grid grid-cols-1 md:grid-cols-3 gap-4 xl:gap-6">
          {/* mt-4 mb-[27px] */}
          <ListBox placeholder="Pilih Kategori" listOption={categories} />
        </div>
        {data.items.map((item, key) => {
          return <SectionCategory dataCategory={item} key={key} />;
        })}
      </section>
    </>
  );
}
