import React from "react";

export default function TalentDetailDesc({ data, type }) {
  console.log(data.talent_riders);
  const handleDetail = () => {
    if (type == "Talent") {
      return {
        title: "Rider",
      };
    }
    if (type == "Influencer") {
      return {
        title: "Detail Tambahan",
      };
    }
    return {
      title: `Alamat ${type}`,
    };
  };

  return (
    <>
      <section className="container-fluid h-max bg-gray-200">
        <div className="w-max h-[55px] xl:h-[70px] flex items-center border-b-3 border-primary-orange">
          <div className=" text-24-responsive font-bold">Detail</div>
        </div>
      </section>
      <section className="container-fluid pt-7 xl:pt-9 pr-detail-partner xxl:w-[1500px]">
        <div className="pb-4 lg:pb-6 xl:pb-8">
          <h1 className="text-18-responsive font-bold">Deskripsi</h1>
          <p className="text-14-responsive">{data?.partner?.description}</p>
        </div>
        <div className="pb-6 lg:pb-10 xl:pb-12">
          <h1 className="text-18-responsive font-bold pb-1">
            {handleDetail().title}
          </h1>
          {(type == "Talent" || type == "Influencer") && (
            <ul className="list-disc list-inside text-18-responsive font-bold">
              {type == "Talent" &&
                data?.talent_riders.map(
                  (rider, key) => {
                    return (
                      <li key={key}>
                        {rider.name}
                        <span className="font-normal">- {rider.details} </span>
                      </li>
                    );
                  }
                  // data?.riderList.map((rider, key) => {
                  //   return (
                  //     <li key={key}>
                  //       {rider.title}
                  //       <span className="font-normal">- {rider.content} </span>
                  //     </li>
                  //   );
                  // }
                )}
              {/* {type == "Influencer" &&
                data.partner.map((detail, key) => {
                  return (
                    <li key={key}>
                      {detail.title}
                      <span className="font-normal">- {detail.content} </span>
                    </li>
                  );
                })} */}
              {type == "Influencer" && (
                <>
                  <li>
                    Status:{" "}
                    <span className="font-normal">
                      {data?.data?.partner?.status_active}
                    </span>
                  </li>
                  <li>
                    Agama:{" "}
                    <span className="font-normal">
                      {data?.data?.religion?.name}
                    </span>
                  </li>
                </>
              )}
            </ul>
          )}
          {type == "Vendor" ||
            (type == "Venue" && (
              <p className="text-14-responsive">
                {data?.data?.partner?.user?.address}
              </p>
            ))}
        </div>
      </section>
    </>
  );
}
