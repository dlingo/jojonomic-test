export const PartnerPrice = (priceMin, priceMax, discount) => {
  const convertPrice = (price, discount) => {
    return ((price * (100 - discount)) / 100)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      .concat(",00");
  };
  const result = (x) => {
    return convertPrice(x, discount);
  };
  return [convertPrice(priceMin,0), result(priceMin), result(priceMax)];
};
