import Stepper from "components/Stepper";

const list = [1, 2, 3];

const HeaderFormBooking = ({ title, note, activeStep = 1 }) => {
  return (
    <>
      <div className="mb-[70px]">
        <Stepper activeStep={activeStep} listStep={list} numbered />
      </div>
      <div className="self-start mb-7">
        <h1 className="text-24-responsive font-bold">{title}</h1>
        <p className="mt-[6px] text-[10px] md:text-xs">{note}</p>
      </div>
    </>
  );
};

export default HeaderFormBooking;
