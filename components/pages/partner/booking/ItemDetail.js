const ItemDetail = ({ value, quantity }) => {
  return (
    <div className="grid grid-cols-2 md:grid-cols-4">
      <h2 className="text-responsive-18 font-bold">Item</h2>
      <div className="flex">
        <p className="mr-6 md:mr-14">:</p>
        <p className="text-responsive-18">
        {value}
        </p>
      </div>
      <h2 className="text-responsive-18 font-bold md:ml-8">Jumlah Item</h2>
      <div className="flex">
        <p className="mr-6 md:mr-14">:</p>
        <p className="text-responsive-18">
        {quantity}
        </p>
      </div>
    </div>
  );
}

export default ItemDetail;
