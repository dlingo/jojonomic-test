import TextField from "@/components/TextField";
import ListBox from "@/components/ListBox";
import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";
import { TrashIcon } from "@heroicons/react/solid";
import { PlusIcon } from "@heroicons/react/outline";

import { Grid } from "@material-ui/core";
import { useForm, useFieldArray } from "react-hook-form";
import useSWR from "swr";
import { fetcher } from "src/utils/fetch";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidation, listItem, timeArr } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepVendor } from "@/src/redux/booking/action";
import { useRouter } from "next/router";
import InputDatePicker from "@/components/DatePicker";

const VendorDetail = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const addValue = {
    name: "",
    quantity: "",
  };

  const defaultValues = {
    vendor_items: partner?.vendor?.vendor_items || [addValue],
    form: {
      event: partner?.vendor?.form?.event || "",
      date: partner?.vendor?.form?.date || "",
      address: partner?.vendor?.form?.address || "",
      notes: partner?.vendor?.form?.notes || "",
    },
    list: { ...partner?.vendor?.list },
  };

  const {
    register,
    control,
    setValue,
    handleSubmit,
    watch,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(schemaValidation),
    defaultValues,
  });


  const { fields, append, remove } = useFieldArray({
    control,
    name: "vendor_items",
  });

  const { data: cities } = useSWR("/reference/cities", fetcher);

  // console.log(watch())

  const handleBooking = (data) => {
    dispatch(stepVendor(data));
    router.push("/partner/vendor/1/divi/payment?step=vendor-order");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking title="Detail Acara" activeStep={1} />
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[54px] px-[43px] ">
        <form id="booking-form" onSubmit={handleSubmit(handleBooking)}>
          <Grid container spacing={2} className="!mb-2">
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Nama Acara"
                placeholder="Masukkan Nama Acara"
                name="form.event"
                required
                register={register}
                error={errors}
                statusError={errors?.form?.event}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <InputDatePicker
                placeholder="Pilih Tanggal Acara"
                label="Tanggal Acara"
                name="form.date"
                control={control}
                required
                error={errors}
                statusError={errors?.form?.date}
                defaultValues={defaultValues.form.date}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <ListBox
                label="Waktu Mulai"
                placeholder="Masukkan Waktu Mulai"
                name="time.start"
                listOption={timeArr || []}
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.time?.start}
                defaultValues={defaultValues?.list?.time?.start}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <ListBox
                label="Waktu Selesai"
                placeholder="Masukkan Waktu Selesai"
                name="time.end"
                listOption={timeArr || []}
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.time?.end}
                defaultValues={defaultValues?.list?.time?.end}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <ListBox
                label="Lokasi Acara"
                placeholder="Pilih Lokasi Acara"
                name="form.city_id"
                listOption={cities?.data || []}
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.form?.city_id}
                defaultValues={defaultValues?.list?.form?.city_id}
              />
            </Grid>
          </Grid>
          {fields.map((field, index) => {
            return (
              <Grid key={field.id} container spacing={2}>
                <Grid item xs={12} md={6}>
                  <ListBox
                    label="Item yang Diinginkan"
                    placeholder="Pilih Item yang Diinginkan"
                    listOption={listItem || []}
                    name={`vendor_items.${index}.name`}
                    selectedName="name"
                    selectedValue="name"
                    setValue={setValue}
                    control={control}
                    defaultValues={
                      defaultValues?.list?.vendor_items?.[index]?.name
                    }
                    // required
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <div className="relative">
                    <TextField
                      label="Jumlah Item"
                      placeholder="Masukkan Jumlah Item"
                      name={`vendor_items.${index}.quantity`}
                      register={register}
                      // required
                    />
                    {index > 0 && (
                      <button
                        type="button"
                        onClick={() => remove(index)}
                        className="absolute top-0 right-0"
                      >
                        <TrashIcon className="w-4 text-red-500 cursor-pointer" />
                      </button>
                    )}
                  </div>
                </Grid>
              </Grid>
            );
          })}
          <Grid container spacing={2}>
            <Grid item xs={12} className="!mb-4">
              <Button
                color="primary-bg"
                className="mt-3 !rounded-lg"
                iconPrefix={<PlusIcon className="w-6" />}
                iconButton
                onClick={() => append(addValue)}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Alamat Acara"
                placeholder="Masukkan Alamat Acara"
                name="form.address"
                required
                type="textarea"
                register={register}
                error={errors}
                statusError={errors?.form?.address}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Catatan untuk Partner"
                placeholder="Masukkan Catatan untuk Partner"
                name="form.notes"
                required
                type="textarea"
                register={register}
              />
            </Grid>
          </Grid>
        </form>
      </div>
      <Button
        form="booking-form"
        className="mt-16 px-0 py-4 rounded-[15px]"
        color={!isValid ? "primary-gray" : "primary-bg"}
        type="submit"
        classText="font-bold"
        text="LANJUTKAN"
      />
    </>
  );
};

export default VendorDetail;
