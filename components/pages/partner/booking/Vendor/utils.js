import * as yup from "yup";

export const schemaValidation = yup.object().shape({
  form: yup.object({
    event: yup.string().required("harus diisi"),
    date: yup.string().required("harus diisi"),
    city_id: yup.string().required("harus diisi"),
    address: yup.string().required("harus diisi"),
  }),
  time: yup.object({
    start: yup.string().required("harus diisi"),
    end: yup.string().required("harus diisi"),
  }),
});

export const listItem = [
  { name: "Nasi Box" },
  { name: "Minuman Soda" },
  { name: "Paket Es Krim dan Desert" },
];

export const timeArr = [
  { name: "01:00" },
  { name: "02:00" },
  { name: "03.00" },
  { name: "04.00" },
  { name: "05.00" },
  { name: "06.00" },
  { name: "07.00" },
  { name: "08.00" },
  { name: "09.00" },
  { name: "10.00" },
  { name: "11.00" },
  { name: "12.00" },
  { name: "13.00" },
  { name: "14.00" },
  { name: "15.00" },
  { name: "16.00" },
  { name: "17.00" },
  { name: "18.00" },
  { name: "19.00" },
  { name: "20.00" },
  { name: "21.00" },
  { name: "22.00" },
  { name: "23.00" },
  { name: "24.00" },
];
