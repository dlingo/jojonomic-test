import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Breadcrumb from "@/components/Breadcrumb";
import BookingProfile from "../BookingProfile";

const VendorDetail = dynamic(import("./VendorDetail"));
const VendorOrder = dynamic(import("./VendorOrder"));
const VendorPayment = dynamic(import("./VendorPayment"));

export default function Vendor() {

  const data = {
    name: "Menu Makanan A",
    price: 50000,
  };

  const router = useRouter();
  const step = router.query?.step;

  const handleStep = () => {
    if (step === "vendor-detail") {
      return <VendorDetail />;
    }
    if (step === "vendor-order") {
      return <VendorOrder />;
    }
    if (step === "vendor-payment") {
      return <VendorPayment />;
    }
    return <div>loading...</div>;
  };
  return (
    <div className="container-content xs:mt-12 lg:mt-[175px]">
      <Breadcrumb />
      <div className="grid grid-cols-1">
          <BookingProfile type="vendor" data={data} activeStep={step} />
          <div className="mt-10 mb-10">
            <div className="h-auto flex flex-col items-center justify-center">
              {handleStep()}
            </div>
          </div>
      </div>
    </div>
  );
}
