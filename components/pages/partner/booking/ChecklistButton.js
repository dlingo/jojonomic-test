import { ChecklistIcon } from "@/components/Icons";

const ChecklistButton = ({ index, id, value, selected }) => {
  const handleClick = () => {
    value(index, id);
  };

  return (
    <>
      <div
        className={`w-4 h-4 md:w-6  md:h-6 rounded-full border-1 border-black 
        ${
          selected
            ? "bg-primary-orange border-none "
            : "hover:bg-primary-orange"
        }
        absolute right-2 md:right-4 lg:right-[30px] cursor-pointer`}
        onClick={handleClick}
      >
        <div className="w-full h-full flex justify-center">
          {selected == true && <ChecklistIcon className="w-4 text-white" />}
        </div>
      </div>
    </>
  );
};

export default ChecklistButton;
