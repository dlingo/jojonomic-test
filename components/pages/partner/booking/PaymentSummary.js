import Button from "@/components/Button";
import PriceFormat from "@/components/PriceFormat";

const PaymentSummary = ({data}) => {
  return (
    <div className="col-span-1">
      <div className="border-2 max-h-[328px] border-black rounded-lg">
        <div className="flex flex-col gap-4 box-content px-4 py-5 border-b-2 h-[150px]">
          <p className="font-bold xl:text-lg text-center">
            Ringkasan Pembayaran
          </p>
          <div className="flex justify-between">
            <p className="text-xs">Total Biaya Sewa Partner</p>
            <PriceFormat className="text-xs font-bold" value={data.price || 750000} />
          </div>
          <div className="flex justify-between">
            <p className="text-xs">Promo Hari Raya</p>
            <PriceFormat className="text-xs font-bold" value={112500} negative />
          </div>
        </div>
        <div>
          <div className="flex flex-col md:gap-6 lg:gap-11 box-content px-4 py-5 h-full">
            <div className="flex justify-between">
              <p className="text-xs font-bold">Total Tagihan</p>
              <PriceFormat
                className="text-xs font-bold text-primary-orange"
                value={500000}
              />
            </div>
            <div className="text-center">
              <Button
                form="booking-form"
                className="mt-3 px-0 py-1 bg-primary-gray text-white"
                // color="primary-bg"
                type="submit"
                classText="md:text-sm"
                text="Pilih Metode Pembayaran"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentSummary;
