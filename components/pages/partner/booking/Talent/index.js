import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Breadcrumb from "@/components/Breadcrumb";
import BookingProfile from "../BookingProfile";

const TalentDetail = dynamic(import("./TalentDetail"));
const TalentOrder = dynamic(import("./TalentOrder"));
const TalentPayment = dynamic(import("./TalentPayment"));

export default function Talent() {
  const data = {
    name: "Divi Tegar",
    price: 850000,
  };

  const router = useRouter();
  const step = router.query?.step;

  const handleStep = () => {
    if (step === "talent-detail") {
      return <TalentDetail />;
    }
    if (step === "talent-order") {
      return <TalentOrder />;
    }
    if (step === "talent-payment") {
      return <TalentPayment />;
    }
    return <div>loading...</div>;
  };


  return (
    <div className="container-content xs:mt-12 lg:mt-[175px]">
      <Breadcrumb />
      <div className="grid grid-cols-1">
        <BookingProfile type="talent" data={data} activeStep={step} />
        <div className="mt-10 mb-10">
          <div className="h-auto flex flex-col items-center justify-center">
            {handleStep()}
          </div>
        </div>
      </div>
      
      {/* <div className="grid xs:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 mt-5 md:gap-2 lg:gap-16">
        <div className="col-span-2 mb-5">
          <BookingProfile type="talent" data={data} activeStep={step} />
          <div className="mt-10 mb-10">
            <div className="h-auto flex flex-col items-center justify-center">
              {handleStep()}
            </div>
          </div>
        </div>

        <PaymentSummary data={data} />
      </div> */}
    </div>
  );
}
