import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";

import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import OrderDetail from "../OrderDetail";
import PaymentDetail from "../PaymentDetail";
import { clearBooking } from "@/redux/booking/action";

const TalentPayment = () => {
  const { booking: partner } = useSelector((state) => state);
  const router = useRouter();
  const dispatch = useDispatch();

  const defaultValue = partner?.talent?.form;
  const handleNext = () => {
    // dispatch(clearBooking());
    router.push("/partner/talent/1/divi/payment?step=talent-detail");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking activeStep={3} />
      </div>
      <h1 className="w-full mt-8 mb-6 text-24-responsive font-bold">
        Order Anda
      </h1>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[24px] px-[43px]">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-y-4">
          <OrderDetail title="Nama Acara" value={defaultValue.event} />
          <OrderDetail title="Tanggal" value={defaultValue.date} />
          <OrderDetail title="Waktu" value={`${defaultValue.time} WIB`} />
          <OrderDetail title="Lokasi" value={defaultValue.city_id} />
          <OrderDetail title="Alamat Lengkap" value={defaultValue.address} />
          <OrderDetail title="Catatan" value={defaultValue.event} />
        </div>
      </div>
      <h1 className="w-full mt-8 mb-6 text-24-responsive font-bold">
        Ringkasan Pembayaran
      </h1>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary pt-[32px] pb-[24px] px-[43px]">
        <div className="grid grid-cols-2 md:grid-cols-5 gap-y-4">
          <PaymentDetail title="Biaya" value={750000} />
          <PaymentDetail title="Discount" value={75000} negative />
          <PaymentDetail title="Pajak 10%" value={75000} />
          <PaymentDetail total title="Total Tagihan" value={750000} />
        </div>
      </div>
      <Button
        className="mt-16 px-0 py-4 rounded-[15px]"
        onClick={handleNext}
        color="primary-bg"
        type="submit"
        classText="font-bold"
        text="BAYAR SEKARANG"
      />
    </>
  );
};

export default TalentPayment;
