import PriceFormat from "@/components/PriceFormat";

const PaymentDetail = ({ title, value, negative, total }) => {
  return (
    <>
      <div className={`md:col-span-3 ${total && "mt-2"}`}>
        <h2
          className={`text-responsive-18 ${
            total ? "text-primary-orange" : "font-bold"
          }`}
        >
          {title}
        </h2>
      </div>
      <div className={`text-right ${total && "mt-3 md:mt-2"}`}>
        <div
          className={`text-[10px] md:text-lg ${
            total ? "text-primary-orange" : "font-bold"
          }`}
        >
          <PriceFormat value={value} negative={negative} />
        </div>
      </div>
      {/* <div></div> */}
    </>
  );
}

export default PaymentDetail;
