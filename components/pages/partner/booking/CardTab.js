import { CreditCardIcon, DiscountIcon } from "@/components/Icons";
import { useState } from "react";
import ChecklistButton from "./ChecklistButton";

const CardTab = ({ data, promo, handler, defaultValue }) => {
  const n = data.data.length;
  const arrNull = new Array(n).fill(false);

  const arrDef = [...arrNull];
  data.data.map((item, index) => {
    if (item.id === defaultValue) {
      arrDef[index] = true;
    }
  });

  const [checkbox, setCheckbox] = useState(arrDef);

  const value = (index, id) => {
    const updatedCheckBox = [...arrNull];
    if (checkbox[index] === false) {
      updatedCheckBox[index] = true;
      handler(id);
    } else {
      handler(undefined);
    }
    setCheckbox(updatedCheckBox);
  };

  const handleStyle = (index) => {
    if (index === 0) {
      return "rounded-t-2xl border-b-1";
    }
    if (index === data.data.length - 1) {
      return "rounded-b-2xl";
    }
    return "border-b-1";
  };
  return (
    <>
      {data.data.map((item, index) => {
        return (
          <div
            className={`w-full xl:w-[773px] p-4 md:p-[30px] bg-primary-white ${handleStyle(
              index
            )} drop-shadow-md border-primary-gray`}
            key={index}
          >
            <div className="flex items-center relative">
              {promo ? (
                <DiscountIcon className="icon-cardtab text-primary-orange" />
              ) : item.icon == "cc" ? (
                <CreditCardIcon className="icon-cardtab text-primary-orange" />
              ) : (
                <div className="icon-cardtab bg-primary-white"></div>
              )}
              {!item.content ? (
                <h1 className="w-[calc(70%)] text-14-responsive font-bold">
                  {item.title}
                </h1>
              ) : (
                <div>
                  <h1 className="text-[10px] md:text-xs font-bold">
                    {item.title}
                  </h1>
                  <p className="text-14-responsive text-primary-gray">
                    {item.content}
                  </p>
                </div>
              )}
              <ChecklistButton
                index={index}
                id={item.id}
                value={value}
                selected={checkbox[index]}
              />
            </div>
          </div>
        );
      })}
    </>
  );
};

export default CardTab;
