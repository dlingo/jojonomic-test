import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";

import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import OrderDetail from "../OrderDetail";
import PaymentDetail from "../PaymentDetail";
import { clearBooking } from "@/redux/booking/action";

const VenuePayment = () => {
  const { booking: partner } = useSelector((state) => state);
  const router = useRouter();
  const dispatch = useDispatch();

  const defaultValue = partner?.venue?.form;
  const handleNext = () => {
    // dispatch(clearBooking());
    router.push("/partner/venue/1/divi/payment?step=venue-detail");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking activeStep={3} />
      </div>
      <h1 className="w-full mt-8 mb-6 text-24-responsive font-bold">
        Order Anda
      </h1>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[24px] px-[43px]">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-y-4">
          <OrderDetail title="Nama Acara" value={defaultValue.event} />
          <OrderDetail title="Tanggal" value={defaultValue.date} date />
          <OrderDetail
            title="Waktu Datang"
            value={`${defaultValue.time} WIB`}
          />
          <OrderDetail title="Alamat Venue" value={`Jl Raya Lembang No.10`} />
          <OrderDetail title="Jumlah Undangan" value={defaultValue.quota} />
          <OrderDetail title="Ruangan" value={defaultValue.room} />
          <OrderDetail title="Catatan" value={defaultValue.notes} />
        </div>
      </div>
      <h1 className="w-full mt-8 mb-6 text-24-responsive font-bold">
        Ringkasan Pembayaran
      </h1>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary pt-[32px] pb-[24px] px-[43px]">
        <div className="grid grid-cols-2 md:grid-cols-5 gap-y-4">
          <PaymentDetail title="Biaya" value={750000} />
          <PaymentDetail title="Discount" value={75000} negative />
          <PaymentDetail title="Pajak 10%" value={75000} />
          <PaymentDetail total title="Total Tagihan" value={750000} />
        </div>
      </div>
      <Button
        className="mt-16 px-0 py-4 rounded-[15px]"
        onClick={handleNext}
        color="primary-bg"
        type="submit"
        classText="font-bold"
        text="BAYAR SEKARANG"
      />
    </>
  );
};

export default VenuePayment;
