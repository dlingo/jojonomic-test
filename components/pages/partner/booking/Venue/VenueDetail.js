import TextField from "@/components/TextField";
import ListBox from "@/components/ListBox";
import InputDatePicker from "@/components/DatePicker";
import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";


import { Grid } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidation, timeArr } from "./utils";
import { stepVenue } from "@/src/redux/booking/action";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

const VenueDetail = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const defaultValues = {
    form: {
      event: partner?.venue?.form?.event || "",
      quota: partner?.venue?.form?.quota || "",
      date: partner?.venue?.form?.date || "",
      room: partner?.venue?.form?.room || "",
      notes: partner?.venue?.form?.notes || "",
    },
    list: { ...partner?.venue?.list },
  };

  const {
    register,
    control,
    setValue,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(schemaValidation),
    mode: "onBlur",
    defaultValues,
  });

  const handleBooking = (data) => {
    dispatch(stepVenue(data));
    router.push("/partner/venue/1/divi/payment?step=venue-order");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking title="Detail Acara" activeStep={1} />
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[54px] px-[43px] ">
        <form id="booking-form" onSubmit={handleSubmit(handleBooking)}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Nama Acara"
                placeholder="Masukkan Nama Acara"
                name="form.event"
                required
                register={register}
                error={errors}
                statusError={errors?.form?.event}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Jumlah Undangan"
                placeholder="Masukkan Jumlah Undangan"
                name="form.quota"
                required
                register={register}
                error={errors}
                statusError={errors?.form?.quota}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <InputDatePicker
                placeholder="Pilih Tanggal Acara"
                label="Tanggal Acara"
                name="form.date"
                required
                control={control}
                error={errors}
                statusError={errors?.form?.date}
                defaultValues={defaultValues.form.date}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <ListBox
                label="Waktu Kedatangan"
                placeholder="Masukkan Waktu Kedatangan"
                name="form.time"
                listOption={timeArr || []}
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.form?.time}
                defaultValues={defaultValues?.list?.form?.time}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Ruangan yang Dipesan"
                placeholder="Pilih Ruangan yang Dipesan"
                name="form.room"
                required
                type="textarea"
                register={register}
                error={errors}
                statusError={errors?.form?.room}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Catatan untuk Partner"
                placeholder="Masukkan Catatan untuk Partner"
                name="form.notes"
                type="textarea"
                register={register}
              />
            </Grid>
          </Grid>
        </form>
      </div>
      <Button
        form="booking-form"
        className="mt-16 px-0 py-4 rounded-[15px]"
        color={!isValid ? "primary-gray" : "primary-bg"}
        type="submit"
        classText="font-bold"
        text="LANJUTKAN"
      />
    </>
  );
};

export default VenueDetail;
