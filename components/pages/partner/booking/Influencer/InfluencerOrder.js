import { useState } from "react";
import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";
import {
  AvatarOutlineIcon,
  LocationIcon,
  PenIcon,
  PhoneIcon,
} from "@/components/Icons";
import OrderDetail from "../OrderDetail";
import ItemDetail from "../ItemDetail";
import ModalEditUser from "../ModalEditUser";

import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

const InfluencerOrder = () => {
  const [openModal, setOpen] = useState(false);
  const handleClickOpenModal = () => {
    setOpen(true);
  };
  const handleClickCloseModal = () => {
    setOpen(false);
  };

  const [userValue, setUser] = useState({
    name: "Wildan J Saputra",
    phone_number: 8128882220,
    address: "Jakarta Selatan",
  });

  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const defaultValue = partner?.influencer?.form;
  const defaultServices = partner?.influencer?.influencer_services;

  console.log(defaultValue);

  const handleNext = () => {
    router.push("/partner/influencer/1/divi/payment?step=influencer-payment");
  };
  const handleBack = () => {
    router.back();
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking activeStep={2} />
      </div>
      <div className="w-full mb-6 flex justify-between">
        <h1 className="text-24-responsive font-bold">Detail Pemesan</h1>
        <div onClick={handleClickOpenModal} className="right-4 cursor-pointer">
          <PenIcon className="w-8 text-primary-orange " />
        </div>
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[32px] px-[43px]">
        <div className="grid grid-cols-1">
          <div className="flex items-center mb-5">
            <AvatarOutlineIcon className="w-6 mr-4 text-primary-orange" />
            <p className="text-responsive-18">{userValue.name} </p>
          </div>
          <div className="flex items-center mb-5">
            <PhoneIcon className="w-6 mr-4 text-primary-orange" />
            <p className="text-responsive-18">{userValue.phone_number}</p>
          </div>
          <div className="flex items-center mb-5">
            <LocationIcon className="w-6 mr-4 text-primary-orange" />
            <p className="text-responsive-18">{userValue.address}</p>
          </div>
        </div>
      </div>
      <div className="w-full mt-8 mb-6 flex justify-between">
        <h1 className="text-24-responsive font-bold">Order Anda</h1>
        <div onClick={handleBack} className="right-4 cursor-pointer">
          <PenIcon className="w-8 text-primary-orange " />
        </div>
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[24px] px-[43px]">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-y-4">
          <OrderDetail title="Nama Barang" value={defaultValue.event} />
          <OrderDetail title="Tanggal" value={defaultValue.date} date />
          <div className="col-span-2 md:col-span-4">
            <h2 className="text-responsive-18 font-bold">Service</h2>
          </div>
          <OrderDetail
            title="yang Harus Dilakukan"
            value={defaultValue.script}
          />
          <OrderDetail title="Detail Barang" value={defaultValue.detail} />
          <OrderDetail title="Catatan" value={defaultValue.notes} />
        </div>
      </div>
      <Button
        className="mt-16 px-0 py-4 rounded-[15px]"
        onClick={handleNext}
        color="primary-bg"
        type="submit"
        classText="font-bold"
        text="LANJUTKAN"
      />

      <ModalEditUser
        openModal={openModal}
        handleClickCloseModal={handleClickCloseModal}
        userValue={userValue}
        setUser={setUser}
      />
    </>
  );
};

export default InfluencerOrder;
