import * as yup from "yup";

export const schemaValidation = yup.object().shape({
    form: yup.object({
        event: yup.string().required("harus diisi"),
        date: yup.string().required("harus diisi"),
        script: yup.string().required("harus diisi"),
        detail: yup.string().required("harus diisi"),
    })
})

export const service = [
  { name: "post IG", id: 3, detail:"3 post seminggu" },
  { name: "story IG", id: 4, detail:"1 story IG" },
  { name: "story Tiktok", id: 5, detail:"2 story tiktok" },
];
