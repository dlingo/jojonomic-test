import { Dialog, DialogTitle } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@/components/TextField";
import Button from "@/components/Button";
import { AvatarOutlineIcon, LocationIcon, PhoneIcon } from "@/components/Icons";
import { useForm } from "react-hook-form";

import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const useStyles = makeStyles((theme) => ({
  paperWidthSm: {
    maxWidth: 520,
    borderRadius: 10,
    margin: 0,
    padding: 52,
    paddingTop: 28,
    paddingBottom: 24,
    [theme.breakpoints.down("sm")]: {
      padding: 20,
    },
  },
}));

const ModalEditUser = ({ openModal, handleClickCloseModal, userValue, setUser }) => {
  const classes = useStyles();

  const defaultValues = {
    user: {
      name: userValue.name,
      phone_number: userValue.phone_number,
      address: userValue.address,
    },
  };

  const validation = yup.object().shape({
    // user: yup.object({
    //   phone_number: yup
    //     .string()
    //     .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
    //     .max(13, "maksimal 13 digit")
    //     .min(9, "minimal 9 digit"),
    // }),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validation),
    mode: "onBlur",
    defaultValues,
  });

  const handleEdit = (data) => {
    // console.log(data.user);
    setUser(data.user)
    handleClickCloseModal();
  };

  return (
    <Dialog
      fullWidth
      open={openModal}
      onClose={handleClickCloseModal}
      aria-labelledby="customized-dialog-title"
      classes={{
        paperWidthSm: classes.paperWidthSm,
      }}
    >
      <DialogTitle>
        <div className="text-responsive-18 text-center">Edit Pemesan</div>
      </DialogTitle>
      <div className="grid grid-cols-1 gap-6">
        <TextField
          label="Nama Lengkap"
          name="user.name"
          register={register}
          error={errors}
          statusError={errors?.user?.name}
          prefixIcon={
            <AvatarOutlineIcon className="w-4 mr-4 text-primary-gray " />
          }
        />
        <TextField
          label="Nomor Telepon"
          name="user.phone_number"
          register={register}
          error={errors}
          statusError={errors?.user?.phone_number}
          prefixIcon={<PhoneIcon className="w-4 mr-4 text-primary-gray " />}
        />
        <TextField
          label="Alamat"
          name="user.address"
          type="textarea"
          register={register}
          error={errors}
          statusError={errors?.user?.address}
          prefixIcon={<LocationIcon className="w-4 mr-4 text-primary-gray " />}
        />
      </div>
      <div className="flex justify-center">
        <Button
          onClick={handleSubmit(handleEdit)}
          className="mt-4 w-[160px] rounded-[15px] mr-4"
          color="primary-bg"
          classText="font-bold"
          text="Simpan"
        />
        <Button
          onClick={handleClickCloseModal}
          className="mt-4 w-[160px] rounded-[15px]"
          color="primary"
          classText="font-bold"
          text="Batal"
        />
      </div>
    </Dialog>
  );
};

export default ModalEditUser;
