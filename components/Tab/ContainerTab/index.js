import React from "react";
import { Tab } from "@headlessui/react";

export default function ContainerTab({ children, containerClass }) {
  return (
    <div className="container-fuild w-full h-16 bg-primary-gray-feed xs:mt-5 xs:mb-12 lg:my-14">
      <Tab.List className={`h-full ${containerClass}`}>{children}</Tab.List>
    </div>
  );
}
