import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Tab,
  Tabs,
  Typography,
  Box,
  Divider,
} from "@material-ui/core/";

import Ulasan from "./Content/Ulasan";

const useStyles = makeStyles(() => ({
  root: {
    minHeight: 450,
    width: 780,
    flexGrow: 1,
    border: "1px solid #000000",
    borderRadius: 10,
    "& .MuiAppBar-root": {
      height: 67,
      backgroundColor: "#23282D",
      borderRadius: "10px 10px 0 0",
      justifyContent: "center",
    },
    "& .MuiTab-wrapper": {
      fontSize: 24,
      fontWeight: 700,
      textTransform: "capitalize",
    },
    "& .Mui-selected": {
      color: "#f6921e",
    },
  },
  divider: {
    //   display: "stat",
    height: 44,
    width: 3,
    backgroundColor: "#fff",
    alignSelf: "center",
  },
}));

export default function DetailPartnerReuse() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="Detail Partner Tabs"
          TabIndicatorProps={{
            style: {
              display: "none",
            },
          }}
        >
          <Tab label="Detail" {...TabProps(0)} />
          <Divider orientation="vertical" className={classes.divider} />
          <Tab label="Ulasan" {...TabProps(1)} />
          <Divider orientation="vertical" className={classes.divider} />
          <Tab label="Galeri" {...TabProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        Item One
      </TabPanel>
      <TabPanel value={value} index={1} />
      <TabPanel value={value} index={2}>
          <Ulasan />
      </TabPanel>
      <TabPanel value={value} index={3} />
      <TabPanel value={value} index={4}>
        Galeri / Daftar Harga / Daftar Ruangan
      </TabPanel>
    </div>
  );
}

// Bagian Fungsi 
function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`detail-partner-tabpanel-${index}`}
      aria-labelledby={`detail-partner-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function TabProps(index) {
  return {
    id: `detail-partner-tab-${index}`,
    "aria-controls": `detail-partner-tabpanel-${index}`,
  };
}
