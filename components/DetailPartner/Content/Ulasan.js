import React from "react";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  container: {
    display: "grid",
    gridTemplateColumns: "1fr 3fr",
  },
  rowProfPic: {
    display: "flex",
  },
  columnProfPic: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  profPic: {
    width: 60,
    height: 60,
    background: "#23282D",
    borderRadius: "50%",
    marginRight: 16,
  },
  columnUlasan: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
  },
}));

export default function Ulasan() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
        {/* mulai dari sini bisa di map */}
      <div className={classes.rowProfPic}>
        <div className={classes.profPic}></div>
        <div className={classes.columnProfPic}>
          <div>DavidBeatt</div>
          <div>2 hari lalu</div>
        </div>
      </div>
      <div className={classes.columnUlasan}>
        <div style={{ color:"#f6921e" }} >Bintang disini</div>
        <div>
          Penampilannya bagus banget garugi deh mesen daffa hehe. Mantap
          pokoknya! Selanjutnya pasti bakal mesen Daffa lg kalo ada acara gini{" "}
        </div>
      </div>
    </div>
  );
}
