import { CircleSpinIcon } from "@/components/Icons";

export default function LoadingOverlay({ loading }) {
  if(!loading) {
    return null
  }
  return (
    <div className="absolute inset-0 flex items-center z-50">
    <div className="absolute inset-0 bg-gray-400 opacity-20" />
      <div className="w-full h-full flex flex-col items-center justify-center space-y-1 relative z-50 ">
        <CircleSpinIcon className="w-5 text-primary-orange" />
        <span className="font-semibold text-xs text-primary-orange">sedang memuat...</span>
      </div>
    </div>
  );
}
