import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 46 60"
    >
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M.5 58.125V7.5A7.5 7.5 0 018 0h30a7.5 7.5 0 017.5 7.5v50.625a1.875 1.875 0 01-2.775 1.646L23 49.01 3.275 59.77A1.877 1.877 0 01.5 58.125zm23.1-42.75a.667.667 0 00-1.2 0l-2.377 4.819a.668.668 0 01-.503.367l-5.325.773a.668.668 0 00-.367 1.136l3.847 3.754a.668.668 0 01.191.592l-.904 5.303a.667.667 0 00.968.701l4.762-2.505a.668.668 0 01.62 0l4.762 2.505a.667.667 0 00.963-.701l-.907-5.306a.668.668 0 01.188-.593l3.854-3.754a.668.668 0 00-.367-1.136l-5.325-.772a.666.666 0 01-.503-.368L23.6 15.375z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Icon;
