import React from "react";

function Icon({ className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 38 38"
    >
      <path
        fill="currentColor"
        d="M32.624.618l-13.79 13.786L5.048.618.452 5.213 14.24 19 .452 32.786l4.596 4.596 13.786-13.787 13.79 13.787 4.595-4.596L23.433 19 37.219 5.213 32.624.618z"
      ></path>
    </svg>
  );
}

export default Icon;
