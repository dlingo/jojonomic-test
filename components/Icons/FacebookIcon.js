import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 15 26"
    >
      <path
        fill="currentColor"
        d="M13.542 14.121l.7-4.543H9.88V6.63a2.272 2.272 0 012.561-2.455h1.982V.307A24.17 24.17 0 0010.905 0c-3.59 0-5.937 2.176-5.937 6.115v3.463H.978v4.543h3.99V25.1h4.911V14.121h3.663z"
      ></path>
    </svg>
  );
}

export default Icon;
