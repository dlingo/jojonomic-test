import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 46 60"
    >
      <path
        fill="currentColor"
        d="M.5 7.5A7.5 7.5 0 018 0h30a7.5 7.5 0 017.5 7.5v50.625a1.874 1.874 0 01-2.914 1.56L23 49.129 3.414 59.685A1.875 1.875 0 01.5 58.125V7.5zM8 3.75A3.75 3.75 0 004.25 7.5v47.123l17.711-9.308a1.875 1.875 0 012.078 0l17.711 9.308V7.5A3.75 3.75 0 0038 3.75H8z"
      ></path>
    </svg>
  );
}

export default Icon;
