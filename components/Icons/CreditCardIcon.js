import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 40 40"
    >
      <path
        fill="currentColor"
        d="M0 10a5 5 0 015-5h30a5 5 0 015 5v12.5H0V10zm28.75 2.5a1.25 1.25 0 00-1.25 1.25v2.5a1.25 1.25 0 001.25 1.25h5A1.25 1.25 0 0035 16.25v-2.5a1.25 1.25 0 00-1.25-1.25h-5zM0 27.5V30a5 5 0 005 5h30a5 5 0 005-5v-2.5H0z"
      ></path>
    </svg>
  );
}

export default Icon;
