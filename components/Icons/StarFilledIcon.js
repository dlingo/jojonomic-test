import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 18 18"
    >
      <path
        fill="currentColor"
        d="M13.643 17.314a.989.989 0 01-.575-.186L9 14.211l-4.067 2.917a.985.985 0 01-1.514-.494.985.985 0 01-.003-.607l1.517-4.896-4.031-2.84a.99.99 0 01.003-1.593.989.989 0 01.576-.19L6.474 6.5l1.589-4.775a.988.988 0 011.875 0L11.5 6.5l5.018.007a.988.988 0 01.58 1.783l-4.031 2.841 1.517 4.896a.985.985 0 01-.94 1.287z"
      ></path>
    </svg>
  );
}

export default Icon;
