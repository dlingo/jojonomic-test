import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 23 26"
    >
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M22.334 25v-2.666A5.334 5.334 0 0017.004 17H6.333A5.334 5.334 0 001 22.334V25M16.998 6.334a5.334 5.334 0 11-10.668 0 5.334 5.334 0 0110.668 0v0z"
      ></path>
    </svg>
  );
}

export default Icon;
