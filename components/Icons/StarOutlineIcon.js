import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 24 22"
    >
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M11.074 1.633c.32-.844 1.531-.844 1.852 0l2.07 5.734a.99.99 0 00.926.633h5.087c.94 0 1.35 1.17.611 1.743L18 13a.968.968 0 00-.322 1.092L19 19.695c.322.9-.72 1.673-1.508 1.119l-4.917-3.12a1 1 0 00-1.15 0l-4.917 3.12c-.787.554-1.83-.22-1.508-1.119l1.322-5.603A.968.968 0 006 13L2.38 9.743C1.64 9.17 2.052 8 2.99 8h5.087a.989.989 0 00.926-.633l2.07-5.734h.001z"
      ></path>
    </svg>
  );
}

export default Icon;
