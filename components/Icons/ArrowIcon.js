import React from "react";

function Icon({ className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 17 27"
    >
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M13.272 0L0 13.272l13.272 13.272 3.095-3.096L6.191 13.272 16.367 3.095 13.272 0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Icon;
