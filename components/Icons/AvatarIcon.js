import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 60 60"
    >
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M40.909 21.818a10.909 10.909 0 11-21.818 0 10.909 10.909 0 0121.818 0zm-5.454 0a5.455 5.455 0 11-10.91 0 5.455 5.455 0 0110.91 0z"
        clipRule="evenodd"
      ></path>
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M30 0C13.432 0 0 13.432 0 30c0 16.568 13.432 30 30 30 16.568 0 30-13.432 30-30C60 13.432 46.568 0 30 0zM5.455 30c0 5.7 1.944 10.947 5.203 15.114a24.507 24.507 0 0119.52-9.66 24.503 24.503 0 0119.341 9.431A24.545 24.545 0 105.455 30zM30 54.545a24.444 24.444 0 01-15.47-5.487 19.069 19.069 0 0115.647-8.149 19.066 19.066 0 0115.516 7.964A24.447 24.447 0 0130 54.545z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Icon;
