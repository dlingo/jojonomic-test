import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

export default function SwiperGaleriModal() {
  const pagination = {
    type: "fraction",
    modifierClass:
      "text-white font-bold xs:text-lg sm:text-xl md:text-2xl lg:text-4xl w-full absolute ",
    renderFraction: function (currentClass, totalClass) {
      return (
        "Foto " +
        '<span class="' +
        currentClass +
        '"></span>' +
        " / " +
        '<span class="' +
        totalClass +
        '"></span>'
      );
    },
  };

  return (
    <>
      <Swiper
        onSlideChange={(e) => console.log(e)}
        onSwiper={(e) => console.log(e)}
        pagination={pagination}
        navigation={true}
      >
        <SwiperSlide>
          <img src="/carousel1.png" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/carousel1.png" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/carousel1.png" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/carousel1.png" />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
