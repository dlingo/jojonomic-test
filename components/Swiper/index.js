import { useState } from "react"
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination, Scrollbar, Thumbs, HashNavigation, Autoplay } from "swiper/core";

SwiperCore.use([Navigation, Pagination, Scrollbar, Thumbs, HashNavigation, Autoplay]);

const SwiperComponent = ({children, ...props}) => {
  return(
    <Swiper {...props}>
      {children}
    </Swiper>
  )
}

export default SwiperComponent