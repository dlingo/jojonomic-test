import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/thumbs";

// import Swiper core and required modules
import SwiperCore, { Navigation, Thumbs } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation, Thumbs]);

const data = [
  {
    img:
      "https://images.unsplash.com/photo-1526510747491-58f928ec870f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
  },
  {
    img:
      "https://images.unsplash.com/photo-1484515991647-c5760fcecfc7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80",
  },
  {
    img:
      "https://images.unsplash.com/photo-1522791786121-a54e01e549c8?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1349&q=80",
  },
  {
    img:
      "https://images.unsplash.com/photo-1486302913014-862923f5fd48?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80",
  },
];

export default function SwiperGaleri() {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <>
      <Swiper
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        className="cursor-pointer w-full mb-9 rounded-lg xs:max-h-[300px] md:max-h-[485px] lg:max-h-[485px]"
      >
        {data.map((item, key) => {
          return (
            <SwiperSlide key={key}>
              <img
                src={item.img}
                alt="dummy"
                className="xs:min-h-[300px] md:max-h-[485px] lg:max-h-[485px] object-cover w-full"
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        spaceBetween={10}
        slidesPerView={4}
        freeMode={true}
        watchSlidesProgress={true}
        className="xs:min-h-[100px] sm:min-h-[150px] md:h-[250] lg:max-h-[250px] lg:min-h-[250px] w-full mySwiper"
      >
        {data.map((item, key) => {
          return (
            <SwiperSlide key={key}>
              <img
                src={item.img}
                alt="dummy"
                className="cursor-pointer rounded-lg object-cover xs:min-h-[100px] xs:max-h-[100px] sm:min-h-[150px] md:max-h-[200] md:min-h-[200px] lg:max-h-[250px] lg:min-h-[250px] w-full"
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </>
  );
}
