import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  typography: {
    fontFamily: "Poppins",
    fontWeight: 700,
    h1: {
      fontSize: "2.25rem",
    },
    poster: {
      fontSize: "2.25rem",
      fontWeight: 800,
      color: "red",
    },
    h2: {
      fontSize: "1.5rem",
    },
    h3: {
      fontSize: "1.125rem",
    },
    h4: {
      fontSize: "0.875rem",
    },
  },
  palette: {
    primary: {
      main: "#F6921E",
    },
  },
  // variants: {
  //     MuiTypography: [
  //       {
  //         props: { variant: 'poster' },
  //         styles: theme.typography.poster,
  //       },
  //     ],
  //   },
});

theme.props = {
  MuiButton: {
    disableElevation: true,
  },
};

theme.overrides = {
  MuiButton: {
    root: {
      borderRadius: 10,
    },
    containedPrimary: {
      color: "#fff",
    },
  },
  MuiOutlinedInput: {
    root: {
      borderRadius: 10,
    },
    // marginDense: {
    //   color: "#C9C9C9",
    //   opacity: 1,
    // },
  },
};

export default theme;
