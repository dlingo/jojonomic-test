import { ThemeProvider } from "@material-ui/styles";
import React from "react";

import AppBar from "../components/AppBar/AppBar";
import Footer from "../components/Footer/Footer";
import Head from "../components/Head/Head";
import globalTheme from "../styles/GlobalTheme";

const Layout = ({ children }) => {
  return (
    <div className="font-pop">
      <ThemeProvider theme={globalTheme}>
        <Head />
        <AppBar />
        <main>{children}</main>
        <Footer />
      </ThemeProvider>
    </div>
  );
};

export default Layout;

// layout global
