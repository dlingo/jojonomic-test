const withPlugins = require("next-compose-plugins");
const path = require('path')

const nextConfig = {
  generateBuildId: async () => {
    return "-dlingo-secret-1234";
  },
  images: {
    domains: [
      "images.unsplash.com",
      "images.pexels.com",
      "eventori.id",
      "www.eventori.id",
      "storage.googleapis.com",
    ],
  },
  trailingSlash: false,
  exportPathMap: function () {
    return {
      "/": { page: "/" },
    };
  },
  env: {
    base_url: process.env.base_url,
  },
};

module.exports = withPlugins([], nextConfig);
