import { useState, useEffect } from "react";
import axios from "axios";
import Cookies from "js-cookie";

const API_GATEWAY = process.env.base_url;

export const Axios = axios.create({
  baseURL: API_GATEWAY,
  timeout: 10000,
});


// request interceptor to add token to request headers
Axios.interceptors.request.use(
  async (config) => {
    const token = await Cookies.get("token");
    // console.log("request server/client: ", config);
    if (token) {
      config.headers = {
        Authorization: `Bearer ${token}`,
      };
    }
    return config;
  },
  (error) => Promise.reject(error),
  null,
  { synchronous: true }
);



export const apiGet = (url, params = {}) =>
  new Promise(async (resolve, reject) => {
    await Axios.get(url, { params })
      .then((res) => {
        resolve({ data: res.data, status: res?.status });
      })
      .catch((err) => {
        reject({ err });
      });
  });
export const apiPost = (url, data) =>
  new Promise(async (resolve, reject) => {
    await Axios.post(url, { ...data })
      .then((res) => {
        resolve({ data: res.data, status: res?.status });
      })
      .catch((err) => {
        reject({ err, status: err?.status });
      });
  });

  export const fetcher = async (...args) => {
    try{
      const fetcher = await Axios(...args);
      return fetcher.data
    } catch (error) {
      return Promise.reject(error);
    }
  }
