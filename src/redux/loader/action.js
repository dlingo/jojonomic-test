import types from "./types";

export const loadingStatus = (key) => {
  return (dispatch) => {
    dispatch({ type: types["LOADING"], key });
  };
};
export const errorStatus = (key, message) => {
  return (dispatch) => {
    dispatch({ type: types["ERROR"], key, message });
  };
};
export const successStatus = (key) => {
  return (dispatch) => {
    dispatch({ type: types["SUCCESS"], key });
  };
};
export const completedStatus = (key) => {
  return (dispatch) => {
    dispatch({ type: types["COMPLETED"], key });
  };
};

