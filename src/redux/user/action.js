import types from "./types";

export const userRegister = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.USER_REGISTER, payload });
  };
};

