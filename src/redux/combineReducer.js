import { combineReducers } from "redux";
import { HYDRATE } from "next-redux-wrapper";
import count from "../redux/count/reducer";
import loader from "../redux/loader/reducer";
import signup from "../redux/signup/reducer";
import user from "../redux/user/reducer";
import booking from "../redux/booking/reducer";

const combineReducer = combineReducers({
  count,
  loader,
  signup,
  user,
  booking,
});

const reducer = (state, action) => {
  // console.log(state, action);
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    // console.log("HYDRATE", nextState);
    // if (state.count.count) nextState.count.count = state.count.count; // preserve count value on client side navigation
    return nextState;
  } else {
    // console.log("ELSE");
    return combineReducer(state, action);
  }
};

export default reducer;
