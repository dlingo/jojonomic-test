import types from "./types";

export const stepTalent = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_TALENT_BOOKING, payload });
  };
};

export const stepVenue = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_VENUE_BOOKING, payload });
  };
};

export const stepVendor = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_VENDOR_BOOKING, payload });
  };
};

export const stepInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_INFLUENCER_BOOKING, payload });
  };
};

export const stepPhotoInfluencer = (payload, index) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_INFLUENCER_BOOKING_PHOTO, payload, index });
  };
};

export const removePhotoInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.REMOVE_PHOTO_INFLUENCER, payload });
  };
};

export const clearBooking = () => {
  return (dispatch) => {
    dispatch({ type: types.CLEAR_BOOKING });
  };
};
