import types from "./types";

const initialState = {
  talent: {},
  influencer: {},
  venue: {},
  vendor: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.STEP_TALENT_BOOKING:
      return {
        ...state,
        talent: {...state.talent, ...action.payload},
        status: "talent",
      };
    case types.STEP_VENUE_BOOKING:
      return {
        ...state,
        venue: {...state.venue, ...action.payload},
        status: "venue",
      };
    case types.STEP_VENDOR_BOOKING:
      return {
        ...state,
        vendor: {...state.vendor, ...action.payload},
        status: "vendor",
      };
    case types.STEP_INFLUENCER_BOOKING:
      return {
        ...state,
        influencer: { ...state.influencer, ...action.payload },
        status: "influencer",
      };
    case types.STEP_INFLUENCER_BOOKING_PHOTO:
      const photoInfluencer = state?.influencer?.photo || [];
      return {
        ...state,
        influencer: {
          ...state.influencer,
          photo: [...photoInfluencer, ...action.payload],
        },
        status: "influencer",
      };
    case types.REMOVE_PHOTO_INFLUENCER:
      let removePhotoInfluencer = state.influencer.photo.filter((_, index) => {
        return index !== action.payload;
      });
      return {
        ...state,
        influencer: {
          ...state.influencer,
          photo: [...removePhotoInfluencer],
        },
      };
    case types.CLEAR_BOOKING:
      return {
        ...initialState,
        status: "clear booking",
      };
    default:
      return state;
  }
}
